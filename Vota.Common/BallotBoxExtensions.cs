//This file is part of Vota.
//
// Copyright 2019 Mathias Tausig <mathias@tausig.at>
//
//Vota is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Vota is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Vota.  If not, see <https://www.gnu.org/licenses/>.
using System;
using System.Collections.Generic;
using System.Linq;

namespace Vota.Common
{
    public static class BallotBoxExtensions
    {
        /// <summary>
        /// Transfers votes away from a candidate to be removed (either because he is already elected or removed) from a collection of voting slips.
        /// If the candidate was the top candidate, he is removed and the weight of the slip is reduced by percentage.
        /// If the candidate was not the top candidate, he is simply removed and the weight remains unaltered.
        /// If the candidate was never part of this slip, the candidates remain the same.
        /// In any case, copy of the slips with the correct candidates are returned.
        /// </summary>
        /// <param name="candidate">The candidate whose votes are to be transferred to the next candidate.</param>
        /// <param name="percentage">Must be larger between 0 and 1.!-- Defaults to 1.</param>
        /// <returns></returns>
        public static IList<VotingSlip> TransferVotes(this IEnumerable<VotingSlip> votingSlips, string candidate, double percentage = 1)
        {
            if(candidate == null)
                throw new ArgumentNullException("candidate");
            if(percentage < 0 || percentage > 1)
                throw new ArgumentException("Percentage must be between 0 and 1.", "percentage");

            List<VotingSlip> newSlips = new List<VotingSlip>();
            foreach(var votingSlip in votingSlips)
                newSlips.Add(votingSlip.TransferVote(candidate, percentage));
            
            return newSlips;
        }

        public static IList<Candidate> GetCandidates(this IEnumerable<VotingSlip> votes)
        {
            if(votes == null)
                throw new ArgumentNullException("votes");

            List<Candidate> candidates = new List<Candidate>();
            foreach(VotingSlip votingSlip in votes)
                candidates.AddRange(votingSlip.Votes.Select(c => new Candidate(c)));
            return candidates.Distinct().ToList();
        }

        /// <summary>
        /// Calculate the number of weighted votes per candidate in a collection of voting slips
        /// </summary>
        /// <param name="votes">The votes to be counted</param>
        /// <returns>A dictionary with the candidate as a key and the weighted votes per candidate.
        /// Every candiate on any slip will be included, even if they hav zero points
        /// </returns>
        public static IDictionary<Candidate, double> GetVotesPerCandidate(this IEnumerable<VotingSlip> votes)
        {
            if(votes == null)
                throw new ArgumentNullException("votes");
            Dictionary<Candidate, double> votesPerCandidate = new Dictionary<Candidate, double>();
                foreach(Candidate candidate in votes.GetCandidates())
                    votesPerCandidate[candidate] = votes.Where(v => v.Votes.Any() && v.Votes.First().Equals(candidate.Name)).Sum(v => v.Weight);
            
            return votesPerCandidate;
        }

        public static IList<VotingSlip> Remove(this IEnumerable<VotingSlip> votingSlips, Candidate candidate)
        {
            if(candidate == null)
                throw new ArgumentNullException("candidate");
            return Remove(votingSlips, candidate.Name);
        }
        
        ///<summary>Removes a (loosing) candidate from a collection of votes without altering the votes</summary>
        ///<returns>A copy of the votes with the candidate removed.</returns>
        public static IList<VotingSlip> Remove(this IEnumerable<VotingSlip> votingSlips, string candidate)
        {
            if(votingSlips == null)
                throw new ArgumentNullException("votingSlips");
            if(candidate == null)
                throw new ArgumentNullException("candidate");
        
            List<VotingSlip> newSlips = new List<VotingSlip>();
            foreach(var votingSlip in votingSlips)
                newSlips.Add(votingSlip.Remove(candidate));
            
            return newSlips;
        }

        ///<summary>Restricts a collection of votes to a set of electable candidates</summary>
        ///<returns>A copy of the votes with the candidates restricted.</returns>
        /// <param name="votingSlips">Voting slips.</param>
        /// <param name="electableCandidates">Electable candidates.</param>
        /// <param name="onlyValidVotes">If set to <c>true</c> all invalid (=empty) voting slips will be dropped</param>
        public static IList<VotingSlip> RestrictCandidates(this IEnumerable<VotingSlip> votingSlips, IEnumerable<Candidate> electableCandidates,
             bool onlyValidVotes = false)
        {
            if(votingSlips == null)
                throw new ArgumentNullException(nameof(votingSlips));
            if(electableCandidates == null)
                throw new ArgumentNullException(nameof(electableCandidates));
        
            var ret = votingSlips.Select(v => v.RestrictCandidates(electableCandidates)).ToList();
            if (onlyValidVotes)
                ret = ret.Where(s => s.Votes.Count > 0).ToList();
            return ret;
        }

        /// <summary>
        /// Select a random candidate from a list.
        /// Must be used if other tie-breaking procedures fail
        /// </summary>
        /// <param name="candidates">The list of candidates to choose one from. Must not be null or empty.</param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException">If <paramref name="candidates"/> is null.</exception>
        /// <exception cref="ArgumentException">If <paramref name="candidates"/> is empty.</exception>
        public static Candidate SelectRandomCandidate(this IEnumerable<Candidate> candidates)
        {
            if(candidates == null)
                throw new ArgumentNullException(nameof(candidates));
            var candidateList = candidates.ToList();
            if (candidateList.Count==0) 
                throw new ArgumentException("candidates must not be empty", nameof(candidates));
            
            
            int randomCandidateNumber = new Random().Next(0, candidateList.Count);
            return candidateList[randomCandidateNumber];
        }


		/// <summary>
		/// Ranks the specified candidates in a given election based on their number of first place selections.
		/// In case of an equal number of first ranks, the second ranks are considered, followed by third ranks and so on, if necessary.
		/// </summary>
		/// <returns>The ranked candidates.</returns>
		/// <param name="votes">The votes </param>
		/// <param name="candidates">The candidates which should be ranked.</param>
		public static IList<Candidate> RankCandidatesOnBestSelections(this IEnumerable<VotingSlip> votes, IEnumerable<Candidate> candidates)
		{
			int maxRank = votes.Max(s => s.Votes.Count);
			Dictionary<Candidate, List<int>> votesOnRank = new Dictionary<Candidate, List<int>>();
			foreach (Candidate candidate in candidates) {
				votesOnRank[candidate] = new List<int>();
				for (int i = 0; i < maxRank; ++i) {
					int votesOnLevel = votes.Count(v => v.Votes.Count > i && v.Votes[i].Equals(candidate.Name));
					votesOnRank[candidate].Add(votesOnLevel);
				}
			}
			var rankComparer = Comparer<Candidate>.Create((c1, c2) => {
				for (int i = 0; i < maxRank; ++i)
					if (votesOnRank[c1][i] != votesOnRank[c2][i])
						return votesOnRank[c1][i].CompareTo(votesOnRank[c2][i]);
				//If this point is reached, all ranks are equal
				var randomSelection = new Candidate[] { c1, c2 }.SelectRandomCandidate();
				if (randomSelection == c1)
					return 1;
				else
					return -1;
			});
			return candidates.OrderByDescending(c => c, rankComparer).ToList();
		}
        
    }
}