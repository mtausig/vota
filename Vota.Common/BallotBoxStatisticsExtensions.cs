//This file is part of Vota.
//
// Copyright 2019 Mathias Tausig <mathias@tausig.at>
//
//Vota is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Vota is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Vota.  If not, see <https://www.gnu.org/licenses/>.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Vota.Common
{
    public static class BallotBoxStatisticsExtensions
    {
        public static string GetRankDistribution(this IEnumerable<VotingSlip> votes,
            IEnumerable<string> candidates = null)
        {
            // If no votes are given, just return an empty string
            if (votes == null || !votes.Any())
                return "";
            
            // Extract the candidates from the votes, if none are explicitely specified
            if (candidates == null || !candidates.Any()) {
                candidates = votes.SelectMany(s => s.Votes).Distinct();
            }

            // Do not calculate statistics for rank 9 and above. This could be made configurable
            int maxRankEvaluated = 8;
            
            // Reduce the max ranke evaluated, of all voting slips are shorter
            int longestSlip = votes.Max(s => s.Votes.Count);
            maxRankEvaluated = Math.Min(longestSlip, maxRankEvaluated);
            
            // Build a dictionary where the number of each rank is stored for the candidates
            // The first entry of the list (index 0) will be the total number of voting slips the candidate is on
            Dictionary<string, List<int>> candidatesRanks = new Dictionary<string, List<int>>();
            
            foreach (var candidate in candidates) {
                var ranks = new List<int>();
                int countOfSlips = votes.Count(s => s.Votes.Contains(candidate));
                ranks.Add(countOfSlips);
                for (int i = 0; i < maxRankEvaluated; ++i) {
                    int rankCount = votes.Count(s => s.Votes.Count > i && s.Votes[i] == candidate);
                    ranks.Add(rankCount);
                }
                candidatesRanks[candidate] = ranks;
            }
            
            var tableHeader = new List<string> {"Name", "Gesamtzahl der Stimmen"};

            //Order the candidates by their share on the voting slips
            candidates = candidates.OrderByDescending(c => candidatesRanks[c].First());
                

            
            for (int i = 0; i < maxRankEvaluated; ++i)
                tableHeader.Add(String.Format("Davon Stimmen im {0}. Rang", i+1));
            var table = new Table(tableHeader);
            foreach (var candidate in candidates) {
                var row = new List<string> {candidate};
                var candidateRanks = candidatesRanks[candidate];
                // Calculate the percentage value of the ranks
                var percentages = candidateRanks.Select(n => (decimal)n).ToArray();
                // First row is the total number of votes -> relative to all votes
                percentages[0] /= votes.Count();
                // Percentages of the ranks are relativ to the candidates votes
                for (int i = 1; i <= maxRankEvaluated; ++i)
                    percentages[i] /= candidateRanks[0];

                row.AddRange(candidateRanks.Select((n, i) => String.Format("{0} ({1:0.##} %)", n, percentages[i]*100)));
                table.AddRow(row);
            }

            return table.ToMarkdownString();
        }

        public static Table GetQuoteDistribution(this IEnumerable<VotingSlip> votes,
            int maxRankBound = 16)
        {
            //remove empty votes
            votes = votes.Where(s => s.Votes.Any());
            int numberOfVotes = votes.Count();
            
            var tableHeader = new string[] {"", "<=20%", "20%-40%", "40%-60%", "60%-80%", ">80%"};
            var table = new Table(tableHeader);
            
            //Convert the votes to the candidate's eligibility for the quote
            var votesQuotes = votes
                .Select(s => s.Votes.Select(c => new Candidate(c).EligibleForQuote))
                .ToList();

            var votesQuotedPercentage = votesQuotes.Select(s => (decimal)s.Count(c => c == true)/s.Count());
            int tier1 = votesQuotedPercentage.Count(s => s <= (decimal)0.2);
            int tier2 = votesQuotedPercentage.Count(s => s > (decimal)0.2 && s <= (decimal)0.4);
            int tier3 = votesQuotedPercentage.Count(s => s > (decimal)0.4 && s <= (decimal)0.6);
            int tier4 = votesQuotedPercentage.Count(s => s > (decimal)0.6 && s <= (decimal)0.8);
            int tier5 = votesQuotedPercentage.Count(s => s > (decimal)0.8);
            string rowTitle = "Ganzer Stimmzettel";
            table.AddRow(new string[]{rowTitle, tier1.ToString(), tier2.ToString(), tier3.ToString(), 
                tier4.ToString(), tier5.ToString()});
            table.AddRow(new string[] {
                String.Format("{0} (%)", rowTitle),
                String.Format("{0}%", Math.Round((decimal)tier1*100/numberOfVotes, 2).ToString()),
                String.Format("{0}%", Math.Round((decimal)tier2*100/numberOfVotes, 2).ToString()),
                String.Format("{0}%", Math.Round((decimal)tier3*100/numberOfVotes, 2).ToString()),
                String.Format("{0}%", Math.Round((decimal)tier4*100/numberOfVotes, 2).ToString()),
                String.Format("{0}%", Math.Round((decimal)tier5*100/numberOfVotes, 2).ToString()),
            });
            for (int maxRank = 2; maxRank <= maxRankBound; maxRank *= 2) {
                var limitedVotesQuotes = votesQuotes.Select(s => s.Take(maxRank));
                votesQuotedPercentage = limitedVotesQuotes.Select(s => (decimal)s.Count(c => c == true)/s.Count());
                tier1 = votesQuotedPercentage.Count(s => s <= (decimal)0.2);
                tier2 = votesQuotedPercentage.Count(s => s > (decimal)0.2 && s <= (decimal)0.4);
                tier3 = votesQuotedPercentage.Count(s => s > (decimal)0.4 && s <= (decimal)0.6);
                tier4 = votesQuotedPercentage.Count(s => s > (decimal)0.6 && s <= (decimal)0.8);
                tier5 = votesQuotedPercentage.Count(s => s > (decimal)0.8);
                rowTitle = String.Format("Top {0}", maxRank.ToString());
                table.AddRow(new string[]{rowTitle, tier1.ToString(), tier2.ToString(), tier3.ToString(), 
                    tier4.ToString(), tier5.ToString()});
                table.AddRow(new string[] {
                    String.Format("{0} (%)", rowTitle),
                    String.Format("{0}%", Math.Round((decimal)tier1*100/numberOfVotes, 2).ToString()),
                    String.Format("{0}%", Math.Round((decimal)tier2*100/numberOfVotes, 2).ToString()),
                    String.Format("{0}%", Math.Round((decimal)tier3*100/numberOfVotes, 2).ToString()),
                    String.Format("{0}%", Math.Round((decimal)tier4*100/numberOfVotes, 2).ToString()),
                    String.Format("{0}%", Math.Round((decimal)tier5*100/numberOfVotes, 2).ToString()),
                });
            }

            return table;
        }
    }
}