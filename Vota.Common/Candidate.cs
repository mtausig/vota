//This file is part of Vota.
//
// Copyright 2019 Mathias Tausig <mathias@tausig.at>
//
//Vota is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Vota is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Vota.  If not, see <https://www.gnu.org/licenses/>.
using System;
using System.Collections.Generic;

namespace Vota.Common
{
    public class Candidate
    {
        public string Name { get; private set; }
        public bool EligibleForQuote {get; private set;}
        
        /// <summary>
        /// Store to cache all candidates created
        /// </summary>
        private static Dictionary<string, Candidate> candidatePool;

        private static Object candidatePoolLock;

        static Candidate()
        {
            candidatePool = new Dictionary<string, Candidate>();
            candidatePoolLock = new Object();
        }
        
        public Candidate(string name, bool eligibleForQuote = false)
        {
            if(name == null)
                throw new ArgumentNullException("name");
            if(name.Length == 0)
                throw new ArgumentException("Name must not be empty", "name");
            
            this.Name = name;
            if (candidatePool.ContainsKey(name)) {
                EligibleForQuote = candidatePool[name].EligibleForQuote;
            } else {
                this.EligibleForQuote = eligibleForQuote;
                lock (candidatePoolLock) {
                    candidatePool[name] = this;
                }
            }
        }

        public override bool Equals(object obj)
        {
            Candidate other = obj as Candidate;
            if(other == null)
                return false;
            // Equality is only defined by name, the EligibleForQuote flag is ignored
            return (Name == other.Name);
        }

        public override int GetHashCode()
        {
            int ret = 0;
         
            ret ^= (Name==null)?0:Name.GetHashCode();
            
            return ret;
        }

        public override string ToString()
        {
            return Name;
        }

    }
}
