//This file is part of Vota.
//
// Copyright 2019 Mathias Tausig <mathias@tausig.at>
//
//Vota is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Vota is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Vota.  If not, see <https://www.gnu.org/licenses/>.
//
// The code is in parts based on the class LinqMarkdownTableExtensions found at 
// https://github.com/jpierson/to-markdown-table/blob/v0.2.1-alpha.3/src/ToMarkdownTable/LinqMarkdownTableExtensions.cs
// ( (c) Jeff, 2017, MIT licensed)

using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace Vota.Common
{
    public class Table
    {
        private List<string> header;
        private List<IList<string>> rows;

        public Table(IEnumerable<string> header)
        {
            if(header == null)
                throw new ArgumentNullException(nameof(header));
            if(!header.Any())
                throw new ArgumentException("Header must not be empty", nameof(header));
            
            this.header = header.ToList();
            rows = new List<IList<string>>();
        }

        public void AddRow(IEnumerable<string> row)
        {
            if(row == null)
                throw new ArgumentNullException(nameof(row));
            if(row.Any(x => x==null))
                throw new ArgumentNullException("row must not contain a null element", nameof(row));
            var rowList = row.ToList();
            //The row must not be too large
            if(rowList.Count > header.Count)
                throw new ArgumentException("Row must not have more fields then the table", nameof(row));
            //If the row is too short, pad it with empty fields
            if (rowList.Count < header.Count) 
                rowList.AddRange(Enumerable.Repeat("", header.Count - rowList.Count));
            rows.Add(rowList);
        }

        public string ToMarkdownString()
        {
            var maxColumnLengths = Enumerable.Range(0, header.Count)
                .Select(i => 
                    rows.Select(r => r[i].Length).Append(header[i].Length)
                        .Max()
                    ).ToList();
            
            StringBuilder table = new StringBuilder();
            
            var headerLine = "| " + string.Join(" | ", header.Select((n, i) => n.PadRight(maxColumnLengths[i]))) + " |";
            table.AppendLine(headerLine);
            
            var headerDataDividerLine = 
                "|" +
                string.Join("|",header.Select((g, i) => new String('-', maxColumnLengths[i] + 2))) + 
                "|";

            table.AppendLine(headerDataDividerLine);

            var lines = rows.Select((row, rowIndex) => String.Format("| {0} |",
                string.Join(" | ", row.Select((cell, i) => cell.PadRight(maxColumnLengths[i])))
                ));

            foreach (var line in lines)
                table.AppendLine(line);

            return table.ToString();
        }
        
        public string ToCsvString()
        {
            StringBuilder csv = new StringBuilder();
            csv.AppendLine(String.Join(";", header));

            foreach (var row in rows)
                csv.AppendLine(String.Join(";", row));
            
            return csv.ToString();
        }
    }
}