//This file is part of Vota.
//
// Copyright 2019 Mathias Tausig <mathias@tausig.at>
//
//Vota is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Vota is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Vota.  If not, see <https://www.gnu.org/licenses/>.
using System;
using System.Collections.Generic;
using System.Linq;
using ServiceStack.Text;

namespace Vota.Common
{
    public class Protocol
    {
        public String Title { get; set; }
        
        private List<object> messages = new List<object>();
        
        private List<Candidate> result = new List<Candidate>();

        public Object[] Messages {
            get { return messages.ToArray(); }
        }

        public string[] Result {
            get { return result.Select(c => c.Name).ToArray();  }
        }
        public Protocol()
        {
            Title = "";
            JsConfig.ExcludeTypeInfo = true;
        }
       
        public void AddMessage(Protocol message)
        {
            messages.Add(message);
        }
        
        public void AddMessage(string message)
        {
            messages.Add(message);
        }
        
        public void AddMessage(string message, params object[] format)
        {
            messages.Add(String.Format(message, format));
        }

		public void PrependMessage(string message)
		{
			messages.Insert(0, message);
		}

		public void PrependMessage(string message, params object[] format)
		{
			messages.Insert(0, String.Format(message, format));
		}

		public void AddElected(Candidate candidate)
        {
            result.Add(candidate);
        }

        public void AddElected(IEnumerable<Candidate> candidates)
        {
            result.AddRange(candidates);
        }

        public string ToJson()
        {
            return JsonSerializer.SerializeToString(this).IndentJson();
        }
    }
}