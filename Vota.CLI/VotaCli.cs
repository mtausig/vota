//This file is part of Vota.
//
// Copyright 2019 Mathias Tausig <mathias@tausig.at>
//
//Vota is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Vota is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Vota.  If not, see <https://www.gnu.org/licenses/>.
﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CommandLine;
using CommandLine.Text;
using ServiceStack.Text;
using Sing;
using Vota.Common;
using CsvReader = Vota.Common.CsvReader;

namespace Vota.CLI
{
	class VotaCli
	{
		class CliOptions
		{
			[Option('f', "listfile", Default = null, HelpText = "A CSV file with the list of votes")]
			public string ListFileName { get; set; }
			[Option('x', "spreadsheetfile", Default = null, HelpText = "A CSV file with the votes based on a spreadsheet")]
			public string SpreadsheetFileName { get; set; }
			[Option('c', "candidates", Default = null, HelpText = "A CSV file with the list of candidates")]
			public string CandidatesFileName { get; set; }
			[Option('s', "seats", Default = (uint)8, HelpText = "The number of seats to be calculated")]
			public uint Seats { get; set; }
			[Option('p', "protocol", Default = null, HelpText = "File where the protocol should be stored. Will be printed to stdout if not specified")]
			public string ProtocolFileName { get; set; }
			[Option('C', "config", Default = null, HelpText = "File where the configuration should be stored. Goes to stdout if not specified")]
			public string ConfigFileName { get; set; }
			[Option('t', "electiontype", Default = ElectionType.RankedSTV, HelpText = "Specify the type of election: STV, QuotedSTV, RankedSTV")]
			public ElectionType ElectionType { get; set; }
			[Option('P', "path", Default = false, HelpText = "Wether to also print the path to the result (all results for seats 1-n).")]
			public bool CalculatePath { get; set; }
			[Option('S', "statistics", Default = null, HelpText = "If present, staistics are written to the file given.")]
			public string StatisticsFileName { get; set; }
			[Option('V', "votecutoff", Default = 0, HelpText = "A number at which all ballot slips shoud be cutoff (not used if <= 0)")]
			public int Cutoff { get; set; }
			[Option('e', "preelected", Default = (uint)0, HelpText = "Number of candidates which have already been elected in previous elections")]
			public uint Preelected { get; set; }
			[Option('E', "preelectedWithQuote", Default = (uint)0, HelpText = "Number of candidates eligible for the quote which have already been elected in previous elections")]
			public uint PreelectedWithQuote { get; set; }
		}

		static void Main(string[] args)
        {
			CliOptions options = null;
			bool showHelp = false;

			var argsParseResult = CommandLine.Parser.Default.ParseArguments<CliOptions>(args);
			argsParseResult.WithParsed<CliOptions>((opts) => options = opts)
					.WithNotParsed<CliOptions>((errs) => {
						// Error message and usage is printed automatically, exit afterwards
						Environment.Exit(1);
					});

            if (options.SpreadsheetFileName != null && !File.Exists(options.SpreadsheetFileName)) {
                Console.Error.WriteLine("ERROR: File '{0}' does not exist.", options.SpreadsheetFileName);
                Environment.Exit(1);
            }
            
            if(options.ListFileName != null && !File.Exists(options.ListFileName)) {
                Console.Error.WriteLine("ERROR: File '{0}' does not exist.", options.ListFileName);
                Environment.Exit(1);
            }
            
            IList<Candidate> candidates = null;
			if (options.CandidatesFileName != null) {
				if (File.Exists(options.CandidatesFileName)) {
					string candidatesCsv = File.ReadAllText(options.CandidatesFileName);
					candidates = CsvReader.ParseCandidates(candidatesCsv);
				} else {
					Console.Error.WriteLine("ERROR: File with candidates '{0}' does not exist.", options.CandidatesFileName);
					Environment.Exit(1);
				}
			} else {
				Console.Error.WriteLine("ERROR: Please specify a file with candidates.");
				showHelp = true;
			}

			if (showHelp) {
				Console.WriteLine(HelpText.AutoBuild(argsParseResult, null, null));
				Environment.Exit(1);
			}

			IElection election = new OfflineElection() {
				Type = options.ElectionType, Seats = options.Seats,
				SeatsPreelected = options.Preelected, SeatsPreelectedWithQuote = options.PreelectedWithQuote,
			};
			if (options.Cutoff > 0)
				election.CutOff = options.Cutoff;

			List<VotingSlip> votingslips = new List<VotingSlip>();

            if (options.ListFileName != null) {
                string csv = File.ReadAllText(options.ListFileName);
                votingslips.AddRange(CsvReader.ParseCsvList(csv, trimEmptyLines:true));
            }
            if (options.SpreadsheetFileName != null) {
                string csv = File.ReadAllText(options.SpreadsheetFileName);
                votingslips.AddRange(CsvReader.ParseCsvSpreadsheet(csv, trimEmptyLines: true));
            }

			if (!votingslips.Any()) {
				Console.Error.WriteLine("ERROR: No votes entered.");
				showHelp = true;
			}

			election.Votes = votingslips;            
            
            if (candidates == null)
                candidates = votingslips.GetCandidates();
			else {
				// A list of candidates was set on input 
				//   -> Issue a warning, if an unknown candidate received a vote
				var unknownCandidatesWithVotes = votingslips.GetCandidates().Except(candidates);
				if(unknownCandidatesWithVotes.Any()) {
					Console.Error.WriteLine("WARNING: Unexpected candidates received a vote.");
					foreach (Candidate unknownCandidate in unknownCandidatesWithVotes) 
						Console.Error.WriteLine("\tUnexpected vote received by '{0}'.", unknownCandidate.Name);					
				}
			}

            election.Candidates = candidates.ToHashSet();

            if (options.ConfigFileName != null)
                File.WriteAllText(options.ConfigFileName, JsonSerializer.SerializeToString(election).IndentJson());
            else
                Console.WriteLine("### Config:\n{0}", JsonSerializer.SerializeToString(election).IndentJson());
            
            Console.WriteLine("### The following candidates are running:\n");
            for(int i=0; i< candidates.Count; ++i)
                Console.WriteLine("{0}.\t{1}{2}", i+1, candidates[i], (candidates[i].EligibleForQuote?(" (*)"):""));

            uint minSeatsForCalculation = options.CalculatePath ? 1 : options.Seats;
            CalculationResult result = null;
            for(uint seatCount=minSeatsForCalculation; seatCount <= options.Seats; ++seatCount) {
                election.Seats = seatCount;
                result = election.CalculateSeats();
                Console.WriteLine("### Result of election ({0} seats) \n", seatCount);
                for(int i=0; i< result.Elected.Count; ++i)
                    Console.WriteLine("{0}.\t{1}", i+1, result.Elected[i]);
            }

			// Add the commandline parameters to the protocol
			Protocol protocol = result.Protocol;
			protocol.PrependMessage("Die Software wurde mit den folgenden Parametern gestartet: {0}", String.Join(" ", args));
            if (options.ProtocolFileName != null)
                File.WriteAllText(options.ProtocolFileName, protocol.ToJson());
            else
                Console.WriteLine("=======\n\nProtocol of count:\n{0}", result.Protocol.ToJson());

            if (options.StatisticsFileName != null) {
                if(candidates != null)
                    votingslips = votingslips.RestrictCandidates(candidates).ToList();
                string statistics = votingslips.GetRankDistribution();
                File.WriteAllText(options.StatisticsFileName, statistics);
                File.AppendAllText(options.StatisticsFileName, votingslips.GetQuoteDistribution(8).ToMarkdownString());
            }
        }
    }
}