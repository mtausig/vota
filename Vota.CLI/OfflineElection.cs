//This file is part of Vota.
//
// Copyright 2019 Mathias Tausig <mathias@tausig.at>
//
//Vota is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Vota is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Vota.  If not, see <https://www.gnu.org/licenses/>.
using System.Collections.Generic;
using System.Runtime.Serialization;
using Sing;
using Vota.Common;

namespace Vota.CLI
{
    [DataContract]
    public class OfflineElection : IElection
    {
        [DataMember]
        public ElectionType Type { get; set; }

        [DataMember]
        public uint Seats { get; set; }
        
        [DataMember]
        public ISet<Candidate> Candidates { get; set; } = new SortedSet<Candidate>();
        
        [DataMember]
        public IList<VotingSlip> Votes { get; set; } = new List<VotingSlip>();

        public int CutOff { get; set; } = 0;

		public uint SeatsPreelected { get; set; }
		public uint SeatsPreelectedWithQuote { get; set; }

		public CalculationResult CalculateSeats()
        {
            StvCounter counter = StvCounterFactory.CreateCounter(Type, Seats, Votes);
            counter.Candidates = Candidates;
            counter.CutOff = CutOff;
            counter.CreateCache();
			counter.SeatsPreelected = SeatsPreelected;
			counter.SeatsPreelectedWithQuote = SeatsPreelectedWithQuote;
            return counter.CalculateSeats(); 
        }
    }
}