# Vota.CLI

The commandline interface to the Vota project

## Input files

The tool requires several files in CSV format top operate. Fields are separated by a semicolon (`;`).

### Candidate listing

The file contains a list of all candidates running in this election.

The first value contains the name, the second (optional) one hast to hold either `1` or `true` if the candidate is eligible for a quote (= female)

Example:

```csv
John Doe
Jane Doe;1
Jack Doe;
Jill Doe;true
```  

### Votes

There are two types of format for the votes file which are accepted: List (`-f`) and Spreadsheet (`-x`).

Whitespaces around the names are always removed.

#### List

Each line simply holds the names of the elected candidates in order of preference.

Example:

```csv
John Doe
Jane Doe; John Doe; Jack Doe; Jill Doe
Jill Doe; Jane Doe;
Jack Doe; John Doe
```

#### Spreadsheet

This file corresponds to votes entered into a spreadsheet.

The first row holds the names of the candidates, the other ones the rank for each candidate.

The first column is either empty or contains a unique identifier of the voting slip.

Example (same votes as above):

```csv
    ;John Doe; Jane Doe; Jack Doe; Jill Doe
A-01; 1
A-02; 2      ; 1       ; 3       ; 4
A-03;        ; 2       ;         ; 1
A-04;2;;1;;
```

## Election types

The tool lets you choose between three different election types:

### STV

A normal implementation of STV, without any quotes.

### QuotedSTV

A variation of STV where for each place after the first, at least half of the spots have to be filled by female candidates (or, in general, ones eligible for a quote).

### RankedSTV (default)

A variation that is both quoted and produces a ranked result, not just an unordered set of elected candidates.

## Output

The tool produces three outputs:

* A *configuration* containing the type and the parameters of the election and the votes
* The *result*: Names of the elected candidates
* A *protocol*: Detailed explanation how the votes are counted, including intermediate results

The result is always printed to stdout, the other two can be redirected to a file

## Usage

The `--help` option displays information about all options:

```bash
$ dotnet Vota.CLI.dll --help 
HELP:
  -f, --listfile=VALUE       A CSV file with the list of votes
  -x, --spreadsheetfile=VALUE
                             A CSV file with the votes based on a spreadsheet
  -c, --candidates=VALUE     A CSV file with the list of candidates
  -s, --seats=VALUE          The number of seats to be calculated
  -p, --protocol=VALUE       File where the protocol should be stored. Will 
                               be printed to stdout if not specified
  -C, --config=VALUE         File where the configuration should be stored. 
                               Goes to stdout if not specified
  -t, --electiontype=VALUE   Specify the type of election: STV, QuotedSTV, 
                               RankedSTV (default)
  -P, --path                 Wether to also print the path to the result (all 
                               results for seats 1-n).
  -S, --statistics=VALUE     If present, staistics are written to the file 
                               given.
  -V, --votecutoff=VALUE     A number at which all ballot slips shoud be 
                               cutoff (not used if <= 0)
  -e, --preelected=VALUE     Number of candidates which have already been 
                               elected in previous elections
  -E, --preelectedWithQuote=VALUE
                             Number of candidates eligible for the quote 
                               which have already been elected in previous 
                               elections
  -h, -?, --help             Print help
  ```
  
A typical usage would be the following:

```bash  
dotnet Vota.CLI.dll -x votes.csv -c candidates.csv -p /tmp/protocol.txt -s 7 -t QuotedSTV
```

## Test files

The root folder contains the files `test_mm_votes.csv` and `test_mm_cand.csv` which can be used as samples.
