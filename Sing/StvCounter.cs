//This file is part of Vota.
//
// Copyright 2019 Mathias Tausig <mathias@tausig.at>
//
//Vota is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Vota is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Vota.  If not, see <https://www.gnu.org/licenses/>.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Sing.Quota;
using Vota.Common;

namespace Sing 
{
    public class StvCounter
    {
        private IEnumerable<VotingSlip> ballotBox;
        protected uint seats;
        IQuota quota = new DroopQuota();

        private IEnumerable<Candidate> candidates;

        protected bool ordered = false;

		protected bool useCache = false;
        protected static CountCache cache = new CountCache();

		private uint seatsPreelected = 0;
		private uint seatsPreelectedWithQuote = 0;

		/// <summary>
		/// The number of total seats which have already been elected in previous elections.
		/// Defaults to 0
		/// </summary>
		public uint SeatsPreelected {
			get => seatsPreelected;
			set {
				if (value < seatsPreelectedWithQuote)
					throw new ArgumentException("Must not be smaller then the number of preelected seats with quote.");
				seatsPreelected = value;
			}
		}

		/// <summary>
		/// The number of already elected candidates eligible for a quote
		/// Defaults to 0
		/// </summary>
		public uint SeatsPreelectedWithQuote {
			get => seatsPreelectedWithQuote;
			set {
				if (value > seatsPreelected)
					throw new ArgumentException("Must not be larger then the number of preelected seats.");
				seatsPreelectedWithQuote = value;
			}
		}

		/// <summary>
		/// If this is a positive value, all ballot slips will be cutoff after the rank specified here.
		/// Defaults to 0 (= not used)
		/// </summary>
		public int CutOff { get; set; } = 0;
        
        public StvCounter(uint seats, IEnumerable<VotingSlip> votes, bool useCache = false)
        {
            if(votes == null)
                throw new ArgumentNullException("votes");
            if(seats == 0)
                throw new ArgumentException("Number of seats must not be zero.", "seats");

            this.seats = seats;
            this.ballotBox = votes.ToList();
            this.Candidates = null;
			this.useCache = useCache;
        }

        ///<summary>Get a copy of the votes for this counter</summary>
        public IEnumerable<VotingSlip> Votes {
            get
            {
                return ballotBox.Select(s => s.RestrictVotes(CutOff)).ToList();
            }
        }

        ///<summary>A set of candidates,which are eligble to be voted.!--
        ///If this is null, there won't be any restrictions
        ///</summary>
        public IEnumerable<Candidate> Candidates {
            get { return candidates; }
            set{
                if(value != null)
                    // The list of electable candiates must not contain duplicates
                    candidates = value.Distinct();
            }
        }

        public bool Ordered {
            get { return ordered; }
        }

        public uint Seats {
            get { return seats; }
        }

        protected string GetProtocolForCachedResultTitle(uint totalSeats) 
        {
            return String.Format("Ergebnis für {0} Sitze mit STV ist bereits bekannt.", totalSeats);
        }

        public virtual CalculationResult CalculateSeats()
        {
            return CalculateSeats(seats);
        }

        protected CalculationResult GetCachedResult(Type counterType, uint totalSeats) 
        {
            if(useCache) {
                var cachedResult = cache.GetResult(GetCacheIdentifier(counterType, totalSeats));
                if(cachedResult != null) {
                    Protocol protocol = new Protocol();
                    protocol.Title = GetProtocolForCachedResultTitle(totalSeats);
                    protocol.AddElected(cachedResult);
                    return new CalculationResult() {Elected = cachedResult, Protocol = protocol};
                }
            }
            //if this point is reached, no cached result is available
            return null;
        }

		protected virtual CountCache.CacheIdentifier GetCacheIdentifier(Type counterType, uint totalSeats)
		{
			return new CountCache.CacheIdentifier {
				Counter = counterType, Seats = totalSeats, Votes = this.Votes,
			};
		}

        protected virtual CalculationResult CalculateSeats(uint totalSeats)
        {
            var cachedResult = GetCachedResult(typeof(StvCounter), totalSeats);
            if(cachedResult != null)
                return cachedResult;
            // If this point is reached, no cached result is available -> Do normal calculation
            Protocol protocol = new Protocol();
            protocol.Title = String.Format("Berechne {0} Sitze mit STV (Startzeit: {1}, Auszählungssoftware: {2} v{3}).",
                totalSeats, DateTime.Now.ToString("dddd yyyy-MM-dd HH:mm:ss"),
                Assembly.GetEntryAssembly().GetName().Name, Assembly.GetEntryAssembly().GetName().Version);
            
            List<Candidate> elected = new List<Candidate>();
            
            IEnumerable<VotingSlip> ballotBox = Votes;
            protocol.AddMessage(String.Format("Es sind {0} Stimmzettel vorhanden.", ballotBox.Count()));
            //Remove all candidates from voting slips which are not electable
            if (Candidates != null)
                ballotBox = ballotBox.RestrictCandidates(Candidates, true);
            //Remove all voting slips which are not valid
            ballotBox = ballotBox.Where(v => v.Votes.Any());

            //Calculate the number of valid votes
            uint validVotes = (uint)ballotBox.Count();
            protocol.AddMessage(String.Format("Es sind {0} gültige Stimmzettel vorhanden.", validVotes));
            
            //Calculate the quota
            double quota = this.quota.CalculateQuota(validVotes, totalSeats);
            protocol.AddMessage("Die Quota für diesen Wahlgang beträgt {0}.", quota);
            //Extract a list of all candidates for the votes
            IList<Candidate> candidates = ballotBox.GetCandidates();
            // List to track all votes that occured
            List<IDictionary<Candidate, double>> countLog = new List<IDictionary<Candidate, double>>();
            
            //Calculate the elected candidates
            while(elected.Count < totalSeats && candidates.Any()) {
                protocol.AddMessage("Beginne Zählung.");
                //Calculate the number of votes for each candidate
                IDictionary<Candidate, double> votesPerCandidate = ballotBox.GetVotesPerCandidate();
                countLog.Add(votesPerCandidate);
                protocol.AddMessage("Ergebnis der Zählung");
                foreach (var candidate in votesPerCandidate.Keys)
                    protocol.AddMessage(String.Format("{0}: {1}", candidate.Name, votesPerCandidate[candidate]));
                
                //Check if the top ranked candidate fulfills the quota
                var orderedVotesPerCandidate = votesPerCandidate.OrderByDescending(kvp => kvp.Value);
                double maxVotes = orderedVotesPerCandidate.First().Value;
                if(maxVotes >= quota) {
                    //The top candidate is elected
                    IEnumerable<Candidate> candidatesWithMaxVotes = votesPerCandidate.Where(kvp => kvp.Value == maxVotes).Select(kvp => kvp.Key);
                    // No need for a tie breaker here. If a candidate has reqached the quote, he will be elected anyway.
                    // Just take the first candidate and the other one(s) in the next iteration
                    Candidate candidateWithMaxVotes = candidatesWithMaxVotes.First();
                    protocol.AddMessage("{0} hat die Quota erreicht und ist gewählt.", candidateWithMaxVotes.Name);
                    elected.Add(candidateWithMaxVotes);
                    double transferPercentage = (maxVotes - quota)/maxVotes;
                    ballotBox = ballotBox.TransferVotes(candidateWithMaxVotes.Name, transferPercentage);
                    candidates.Remove(candidateWithMaxVotes);
                } else {
                    //No candidate fulfills the quota -> eliminate the last one
                    protocol.AddMessage("Niemand hat die Quota erreicht.");
                    double minVotes = orderedVotesPerCandidate.Last().Value;
                    IEnumerable<Candidate> candidatesWithMinVotes = votesPerCandidate.Where(kvp => kvp.Value == minVotes).Select(kvp => kvp.Key);
                    protocol.AddMessage("Folgende KandidatInnen haben nur die Minimalzahl an Stimmen ({0}) erreicht: {1}", 
                        minVotes, String.Join(", ", 
                            candidatesWithMinVotes.Select(c => c.Name)));
                    // Tie breaker if multiple candidates have the same (minimal) number of votes:
                    // 1. Preference for candidate which had more votes in a previous count
                    // 2. Random choice
                    Candidate candidateWithMinVotes = null;
                    if (candidatesWithMinVotes.Count() > 1) {
                        int countNumber = countLog.Count - 2;
                        while (countNumber >= 0 && candidatesWithMinVotes.Count() > 1) {
                            //Filter out only the candidates with minimal votes from the count under observation
                            var count = countLog[countNumber]
                                .Where(c => candidatesWithMinVotes.Contains(c.Key))
                                .OrderByDescending(kvp => kvp.Value);
                            double minVotesInCount = count.Last().Value;
                            var candidatesWithMoreVotes =
                                count.Where(kvp => kvp.Value > minVotesInCount).Select(kvp => kvp.Key).ToList();
                            candidatesWithMinVotes =
                                candidatesWithMinVotes.Where(c => !(candidatesWithMoreVotes.Contains(c)));
                            countNumber--;
                        }

                        if (candidatesWithMinVotes.Count() == 1) {
                            // Tie-breaker was successfull
                            candidateWithMinVotes = candidatesWithMinVotes.First();
                            protocol.AddMessage("Nach Berücksichtigung der Stimmen bei den vergangenen Zählrunden scheidet aus: {0}",candidateWithMinVotes.Name);
                        } else {
                            // Tie-breaker was not successfull -> Random choice}
                            protocol.AddMessage(
                                "Nach Berücksichtigung der Stimmen bei den vergangenen Zählrunden verbleiben mit der minimalen Anzahl an Stimmen: {0}",
                                String.Join(", ", candidatesWithMinVotes.Select(c => c.Name)));
                            candidateWithMinVotes = candidatesWithMinVotes.SelectRandomCandidate();
                            protocol.AddMessage("Nach Losentscheid scheidet aus: {0}", candidateWithMinVotes);
                        }
                    } else {
                        // Only one candidate has the minimal number of votes
                        candidateWithMinVotes = candidatesWithMinVotes.First();
                        protocol.AddMessage("{0} scheidet aus.", candidateWithMinVotes.Name);
                    }
                    candidates.Remove(candidateWithMinVotes);
                    ballotBox = ballotBox.Remove(candidateWithMinVotes);
                }
                //Filter out all votes which have become empty
                ballotBox = ballotBox.Where(v => v.Votes.Any());
            }
            protocol.AddElected(elected);
            if(useCache)
				cache.AddResult(GetCacheIdentifier(typeof(StvCounter), totalSeats), elected);
			return new CalculationResult() {Elected = elected, Protocol = protocol};
        }

        ///<summary> Adds a new cache for counts from this counter</summary>
        public void CreateCache()
        {
            cache = new CountCache();
			useCache = true;
        }
    }
}