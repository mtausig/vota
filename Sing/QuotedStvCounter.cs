//This file is part of Vota.
//
// Copyright 2019 Mathias Tausig <mathias@tausig.at>
//
//Vota is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Vota is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Vota.  If not, see <https://www.gnu.org/licenses/>.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Vota.Common;

namespace Sing
{
    public class QuotedStvCounter : StvCounter
    {
        public QuotedStvCounter(uint seats, IEnumerable<VotingSlip> votes, bool useCache = false) : base(seats, votes, useCache)
        {
        }



        /// <summary>
        /// Calculate the number of seats which must be given to candidates eligible for the quote.
        /// A single seat is always open, otherwise at least half of the total seats are reserved for quoted people.
        /// Optionally heeds the values of preelected seats.
        /// </summary>
        /// <param name="totalSeats">The total number of seats.</param>
        /// <returns></returns>
        public static uint CalculateQuotedSeats(uint totalSeats, uint seatsPreelected = 0, uint seatsPreelctedWithQuote = 0)
        {
            if (seatsPreelctedWithQuote > seatsPreelected)
                throw new ArgumentException("Values for preelcted seats are contradictory.", nameof(seatsPreelected));
            totalSeats += seatsPreelected;
			if (totalSeats <= 1)
				return 0;
			else {
				uint quotedSeats = (uint)Math.Ceiling((double)totalSeats / 2);
				quotedSeats = (uint)Math.Max((int)quotedSeats - seatsPreelctedWithQuote, 0);
				return quotedSeats;
			}
        }

        /// <summary>
        /// Calculate the number of seats which must be given to candidates eligible for the quote.
        /// A single seat is always open, otherwise at least half of the total seats are reserved for quoted people.
        /// Heeds the values of preelected seats set in the corresponding properties
        /// </summary>
        /// <param name="totalSeats">The total number of seats.</param>
        /// <returns></returns>
        protected uint CalculateQuotedSeats(uint totalSeats)
        {
            return CalculateQuotedSeats(totalSeats, SeatsPreelected, SeatsPreelectedWithQuote);
        }

        /// <summary>
        /// Calculate the maximum number of open seats in a setting.
        /// Optionally heeds the values of preelected seats.
        /// </summary>
        /// <param name="totalSeats">The maximum total number of seats</param>
        /// <param name="quotedSeatsElected">Cap to the quoted seats (like the already elected ones or maximum number running)</param>
        /// <returns></returns>
        public static uint CalculateOpenSeats(uint totalSeats, uint quotedSeatsElected, uint seatsPreelected, uint seatsPreelectedWithQuote)
        {
            if (seatsPreelectedWithQuote > seatsPreelected)
                throw new ArgumentException("Values for preelcted seats are contradictory.", nameof(seatsPreelected));
                        
            uint quotedSeats = CalculateQuotedSeats(totalSeats, seatsPreelected, seatsPreelectedWithQuote);
			if (quotedSeats <= quotedSeatsElected)
				return (uint)Math.Max(0, (int)totalSeats - quotedSeatsElected);
            else {
                int maxTotalSeats = (int)Math.Min(totalSeats+seatsPreelected, (quotedSeatsElected+seatsPreelectedWithQuote) * 2);
				return (uint)Math.Max(0, (maxTotalSeats - quotedSeatsElected - seatsPreelected));
			}
        }

        /// <summary>
        /// Calculate the maximum number of open seats in a setting.
        /// Heeds the values of preelected seats set in the corresponding properties
        /// </summary>
        /// <param name="totalSeats">The maximum total number of seats</param>
        /// <param name="quotedSeatsElected">Cap to the quoted seats (like the already elected ones or maximum number running)</param>
        /// <returns></returns>
        public uint CalculateOpenSeats(uint totalSeats, uint quotedSeatsElected)
        {
            return CalculateOpenSeats(totalSeats, quotedSeatsElected, SeatsPreelected, SeatsPreelectedWithQuote);
        }

        protected new string GetProtocolForCachedResultTitle(uint totalSeats) 
        {
            return String.Format("Ergebnis für {0} quotierte Sitze mit STV ist bereits bekannt.", totalSeats);
        }

		protected override CountCache.CacheIdentifier GetCacheIdentifier(Type counterType, uint totalSeats)
		{
			return new CountCache.CacheIdentifier {
				Counter = counterType, Seats = totalSeats, Votes = this.Votes,
				PreelectedSeats = this.SeatsPreelected, PreelectedSeatsWithQuote = this.SeatsPreelectedWithQuote,
			};
		}

		protected override CalculationResult CalculateSeats(uint totalSeats)        
        {
            var cachedResult = GetCachedResult(typeof(QuotedStvCounter), totalSeats);
            if(cachedResult != null)
                return cachedResult;
            // If this point is reached, no cached result is available -> Do normal calculation
            Protocol protocol = new Protocol();
            protocol.Title = String.Format("Berechne {0} quotierte Sitze mit STV (Startzeit: {1}, Auszählungssoftware: {2} v{3}).",
                totalSeats, DateTime.Now.ToString("dddd yyyy-MM-dd HH:mm:ss"),
                Assembly.GetEntryAssembly().GetName().Name, Assembly.GetEntryAssembly().GetName().Version);
            
            protocol.AddMessage("Berechne die Sitze unquotiert.");
            var baseCalculation = base.CalculateSeats(totalSeats);
            var elected = baseCalculation.Elected;
            protocol.AddMessage(baseCalculation.Protocol);
            
            // The size of elected could be smaller then this.seats -> calculate the quote size from this value
            uint quotedSeats = CalculateQuotedSeats(totalSeats);
            var electedWithQuote = elected.Where(c => c.EligibleForQuote);
            protocol.AddMessage("Die Zählung ergab {0} gewählte Personen, davon {1} quotierte. Es sind {2} quotierte Personen notwendig.",
                elected.Count, electedWithQuote.Count(), quotedSeats);
            if (electedWithQuote.Count() >= quotedSeats) {
                // Quote already fulfills, nothing to do
                protocol.AddMessage("Quote erfolgreich erfüllt.");
                protocol.AddElected(elected);
                return new CalculationResult() {Elected = elected, Protocol = protocol};
            } else {
                // Quote not fulfilled -> Fix that
                protocol.AddMessage("Die Basiszählung erfüllt die Quote nicht.");
				// Calculate with as many seats as necessary until the quote is fulfilled
				uint increasedSeats = totalSeats;
				IList<Candidate> lastElected;
				IList<Candidate> newElected = elected;
				CalculationResult newCalculationResult;
				do {
					lastElected = newElected;
					increasedSeats++;
					protocol.AddMessage("Führe eine Zählung für {0} Personen durch.", increasedSeats);
					newCalculationResult = base.CalculateSeats(increasedSeats);
					protocol.AddMessage(newCalculationResult.Protocol);
					newElected = newCalculationResult.Elected;
					electedWithQuote = newElected.Where(c => c.EligibleForQuote);
					protocol.AddMessage("Nach dieser Zählung sind {0} quotierte Personen gewählt.", electedWithQuote.Count());
					if (electedWithQuote.Count() > quotedSeats) {
						// Too many quoted candidates elected -> apply tiebreaker to the latest additions
						var lastElectedWithQuote = lastElected.Where(c => c.EligibleForQuote);
						IList<Candidate> latestElectedWithQuote = electedWithQuote.Except(lastElectedWithQuote).ToList();
						int quotedCandidatesNeededFromLastCount = (int)quotedSeats - lastElectedWithQuote.Count();
						protocol.AddMessage("Zuviele quotierte Personen in der letzten Zählung gewählt. Es müssen {0} aus den folgenden ausgewählt werden: {1}",
							quotedCandidatesNeededFromLastCount, String.Join(", ", latestElectedWithQuote.Select(c => c.Name).ToArray()));
						latestElectedWithQuote = Votes.RankCandidatesOnBestSelections(latestElectedWithQuote);
						var topRanked = latestElectedWithQuote.Take(quotedCandidatesNeededFromLastCount);
						protocol.AddMessage("Basierend auf der Anzahl der meisten Erststimmen (bzw. nachfolgender Präferenzen) wurden ausgewählt: {0}",
							String.Join(", ", topRanked.Select(c => c.Name).ToArray()));
						var cutCandidates = latestElectedWithQuote.Skip(quotedCandidatesNeededFromLastCount);
						newElected = newElected.Except(cutCandidates).ToList();
						electedWithQuote = newElected.Where(c => c.EligibleForQuote);
					}
				} while ((newElected.Count() > lastElected.Count()) && (electedWithQuote.Count() < quotedSeats));
				// Calculation of quoted seats finished
				protocol.AddMessage("Folgende Personen sind auf quotierte Plätze gewählt: {0}",
                    String.Join(", ", electedWithQuote));

				if (electedWithQuote.Count() < quotedSeats) {
					// Not enough quoted candidates could be elected
					protocol.AddMessage("Es konnten nur {0} quotierte Plätze gewählt werden. Die Gesamtzahl wird entsprechend angepasst.");
				}

                uint openSeats = CalculateOpenSeats(totalSeats, (uint)electedWithQuote.Count());
                protocol.AddMessage("Es verbleiben {0} offene Plätze.", openSeats);
                
                // Calculate seats with decreasing size until no more than openSeats remain (apart from the already elected)
                uint decreasedSeats = totalSeats;
                protocol.AddMessage("Führe eine Zählung für {0} Personen durch.", decreasedSeats);
                newCalculationResult = base.CalculateSeats(decreasedSeats);
                protocol.AddMessage(newCalculationResult.Protocol);
                newElected = newCalculationResult.Elected;
                var electedOnOpenSeats = newElected.Except(electedWithQuote);
                protocol.AddMessage("Unter den hier gewählten befinden sich {0} noch nicht fixierte Personen.",
                    electedOnOpenSeats.Count());
                while (electedOnOpenSeats.Count() > openSeats && decreasedSeats > 0) {
                    decreasedSeats--;
                    protocol.AddMessage("Führe eine Zählung für {0} Personen durch.", decreasedSeats);
                    newCalculationResult = base.CalculateSeats(decreasedSeats);
                    protocol.AddMessage(newCalculationResult.Protocol);
                    newElected = newCalculationResult.Elected;
                    electedOnOpenSeats = newElected.Except(electedWithQuote);
                    protocol.AddMessage("Unter den hier gewählten befinden sich {0} noch nicht fixierte Personen.",
                        electedOnOpenSeats.Count());
                }
                // Calculation for open seats finished
                protocol.AddMessage("Auf die offenen Plätze wurden somit gewählt: {0}", String.Join(", ", electedOnOpenSeats));
                
                elected = electedWithQuote.Concat(electedOnOpenSeats).ToList();
                protocol.AddElected(elected);
				if (elected.Count > totalSeats) {
					// Sanity check. This really should never happen.
					throw new StvCountException(String.Format("Sanity check failed. Too many seats calculated ({0} instead of {1}).",
						elected.Count, totalSeats));
				}
				if(useCache)
                    cache.AddResult(GetCacheIdentifier(typeof(QuotedStvCounter), totalSeats), elected);
                return new CalculationResult() {Elected = elected, Protocol = protocol};
            }
        }
    }
}