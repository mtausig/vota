//This file is part of Vota.
//
// Copyright 2019 Mathias Tausig <mathias@tausig.at>
//
//Vota is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Vota is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Vota.  If not, see <https://www.gnu.org/licenses/>.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using Vota.Common;


namespace Sing
{
    public class RankedStvCounter : QuotedStvCounter
    {
        public RankedStvCounter(uint seats, IEnumerable<VotingSlip> votes, bool useCache = false) : base(seats, votes, useCache)
        {
            ordered = true;
        }

        protected new string GetProtocolForCachedResultTitle(uint totalSeats) 
        {
            return String.Format("Ergebnis für {0} gereihte Sitze mit STV ist bereits bekannt.", totalSeats);
        }

        protected override CalculationResult CalculateSeats(uint totalSeats)
        {
            var cachedResult = GetCachedResult(typeof(RankedStvCounter), totalSeats);
            if(cachedResult != null)
                return cachedResult;
            // If this point is reached, no cached result is available -> Do normal calculation
            Protocol protocol = new Protocol();
            protocol.Title = String.Format("Berechne {0} gereihte Sitze mit STV (Startzeit: {1}, Auszählungssoftware: {2} v{3}).",
                totalSeats, DateTime.Now.ToString("dddd yyyy-MM-dd HH:mm:ss"),
                    Assembly.GetEntryAssembly().GetName().Name, Assembly.GetEntryAssembly().GetName().Version);
            
            List<Candidate> elected = new List<Candidate>();
            int candidateCount;
            if (Candidates != null)
                candidateCount = Candidates.Count();
            else
                candidateCount = Votes.GetCandidates().Count;

            while (elected.Count < totalSeats) {
                uint nextSeatNumber = (uint) elected.Count + 1;
                protocol.AddMessage("Berechne {0}. Platz.", nextSeatNumber);
                uint quotedSeatsNeeded = this.CalculateQuotedSeats(nextSeatNumber);
                bool nextSeatQuoted = (elected.Where(c => c.EligibleForQuote).Count() < quotedSeatsNeeded);
                protocol.AddMessage("Dieser Platz muss quotiert sein: {0}", nextSeatQuoted?"Ja":"Nein");
                Candidate newCandidate = null;
                for (uint n = nextSeatNumber; n <= candidateCount; ++n) {
					IEnumerable<Candidate> newCandidates = new Candidate[0];
					for (uint quorumReduction = 0; quorumReduction <= SeatsPreelected; ++quorumReduction) {
						protocol.AddMessage("Berechne ungereihte Auswahl von {0} Personen.", n + quorumReduction);
						var newCountResult = base.CalculateSeats(n + quorumReduction);
						protocol.AddMessage(newCountResult.Protocol);
						var newCount = newCountResult.Elected;
						newCandidates = newCount.Except(elected);
						if (newCandidates.Any())
							// We have found a possible candidate -> break the loop and find the right one
							break;
					}
					if(!newCandidates.Any()) {
                        // No new candidates elected -> end counting
                        protocol.AddMessage("Keine neue Person konnte ausgewählt werden. Beende den Wahlgang.");
                        break;
                    }
                    protocol.AddMessage("Davon sind {0} noch nicht fix gewählt: {1}.",
                        newCandidates.Count(), String.Join(", ", newCandidates));
                    // Filter out candidates not eligible for quote if the next seat is quoted
                    if (nextSeatQuoted) {
                        newCandidates = newCandidates.Where(c => c.EligibleForQuote);
                        protocol.AddMessage("Davon können {0} auf diesen quotierten Platz gewählt werden: {1}.",
                            newCandidates.Count(), String.Join(", ", newCandidates));
                    }

                    if (newCandidates.Any()) {
                        // Tie-Breaker: Which candidate has more first (second, ...) level votes
                        int tieBreakerLevel = 1;
                        while (newCandidates.Count() > 1) {
                            protocol.AddMessage("Mehrere Personen kommen für diesen Listenplatz in Frage: {0}", 
                                String.Join(", ", newCandidates.Select(c => c.Name)));
                            var votesOnLevel = newCandidates.Select(c => new KeyValuePair<Candidate, int>(c,
                                    Votes.Count(v => v.Votes.Count >= tieBreakerLevel && v.Votes[tieBreakerLevel - 1].Equals(c.Name))
                                ));
                            int maxVotesOnLevel = votesOnLevel.Select(kvp => kvp.Value).Max();
                            protocol.AddMessage("Die Höchstanzahl an Stimmen im {0}. Rang unter diesen Personen beträgt {1}.",
                                tieBreakerLevel, maxVotesOnLevel);
                            if (maxVotesOnLevel == 0)
                                //No votes anymore on that level -> Tie breaking failed -> make a random choice below
                                break;
                            var newCandidatesWithMaxVotesOnLevel = votesOnLevel
                                .Where(kvp => kvp.Value == maxVotesOnLevel).Select(kvp => kvp.Key);
                            newCandidates = newCandidatesWithMaxVotesOnLevel.ToList();
                            protocol.AddMessage("Die Höchstanzahl an Stimmen im {0}. Rang haben erreicht: {1}.",
                                tieBreakerLevel, String.Join(", ", newCandidates));
                            tieBreakerLevel++;
                        }
                        if (newCandidates.Count() > 1) {
                            // Tie breaking failed above -> make a random choice
                            protocol.AddMessage("Keine deterministische Entscheidung möglich. Führe einen Lostentscheid durch zwischen: ",
                                String.Join(", ", newCandidates));
                            newCandidate = newCandidates.SelectRandomCandidate();
                            protocol.AddMessage("Den Losentscheid für den {0}. Platz hat gewonnen: {1}.",
                                nextSeatNumber, newCandidate);
                        }else
                            newCandidate = newCandidates.First();
                            protocol.AddMessage("Auf den {0}. Platz gewählt: {1}.", 
                                nextSeatNumber, newCandidate);
                        // We have a new candidate -> leave the for loop and add it to the list of elected candidates 
                        break;
                    }
                }

                if (newCandidate != null)
                    elected.Add(newCandidate);
                else {
                    // No candidate found -> abort the count
                    protocol.AddMessage("Es konnte keine KandidatIn für den {0}.Platz bestimmt werden. Wahlgang wird abgebrochen.", nextSeatNumber);
                    break;
                }
            }

            //Count finished. Either because we have enough candidates or because the count had to be aborted
            protocol.AddElected(elected);
            if(useCache)
				cache.AddResult(GetCacheIdentifier(typeof(RankedStvCounter), totalSeats), elected);
			return new CalculationResult() {Elected = elected, Protocol = protocol};
        }
    }
}