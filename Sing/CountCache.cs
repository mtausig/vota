//This file is part of Vota.
//
// Copyright 2019 Mathias Tausig <mathias@tausig.at>
//
//Vota is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Vota is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Vota.  If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using Vota.Common;

namespace Sing 
{
    ///<summary>
    /// Class which can cache STV counting results to avoid redudantant recalculations
    ///</summary>
    public class CountCache
    {
        public class CacheIdentifier
        {
            public IEnumerable<VotingSlip> Votes { get; set; } = new VotingSlip[0];
            public Type Counter { get; set; }
            public uint Seats { get; set; }
			public uint PreelectedSeats { get; set; } = 0;
			public uint PreelectedSeatsWithQuote { get; set; } = 0;

			public override bool Equals(object obj)
            {
                CacheIdentifier other = obj as CacheIdentifier;
                if (other == null)
                    return false;
                return (this.Counter == other.Counter) 
                    && this.Seats == other.Seats
                    && this.Votes.SequenceEqual(other.Votes)
					&& this.PreelectedSeats == other.PreelectedSeats
					&& this.PreelectedSeatsWithQuote == other.PreelectedSeatsWithQuote
					;
            }

            public override int GetHashCode()
            {
                int ret = 0;
                string votesString = String.Join("##", Votes.Select(v => v.GetHashCode()).ToArray());
                ret ^= votesString.GetHashCode();

                ret ^= Counter.GetHashCode();
                ret ^= Seats.GetHashCode();
				ret ^= PreelectedSeats.GetHashCode();
				ret ^= PreelectedSeatsWithQuote.GetHashCode();

				return ret;
            }

        }

        private Dictionary<CacheIdentifier, IList<Candidate>> cache;

        public CountCache()
        {
            cache = new Dictionary<CacheIdentifier, IList<Candidate>>();
        }

		///<summary>
		/// Try to get a caches result for some identifier
		///</summary>
		///<returns> The cached result or null if none is available</returns>
		public IList<Candidate> GetResult(CacheIdentifier cacheIdentifier)
		{
			IList<Candidate> result = null;
			if (cache.TryGetValue(cacheIdentifier, out result))
				return result;
			else
				return null;
		}

		///<summary>
		/// Try to get a caches result for a specific counter
		///</summary>
		///<returns> The cached result or null if none is available</returns>
		[Obsolete()]
		public IList<Candidate> GetResult(StvCounter counter) 
        {
            return GetResult(counter, counter.Seats);
        }

		///<summary>
		/// Try to get a caches result for a specific counter and a specified number of seats
		///</summary>
		///<returns> The cached result or null if none is available</returns>
		[Obsolete()]
		public IList<Candidate> GetResult(StvCounter counter, uint seats) 
        {
            return GetResult(counter.GetType(), seats, counter.Votes);
        }

		///<summary>
		/// Try to get a caches result for a specific counter and a specified number of seats
		///</summary>
		///<returns> The cached result or null if none is available</returns>
		[Obsolete()]
		public IList<Candidate> GetResult(Type counterType, uint seats, IEnumerable<VotingSlip> votes) 
        {
			var dictKey = new CacheIdentifier { Counter = counterType, Seats = seats, Votes = votes };
			return GetResult(dictKey);
        }

		///<summary>
		/// Cache a specified result.
		/// Previous results for the same counter will be overwritte (if present).
		///</summary>
		[Obsolete()]
		public void AddResult(StvCounter counter, IList<Candidate> result) 
        {
            AddResult(counter, result, counter.Seats);
        }

		///<summary>
		/// Cache a specified result.
		/// Previous results for the same counter will be overwritte (if present).
		///</summary>
		[Obsolete()]
		public void AddResult(StvCounter counter, IList<Candidate> result, uint seats) 
        {
            AddResult(counter.GetType(), result, seats, counter.Votes);
        }

		///<summary>
		/// Cache a specified result.
		/// Previous results for the same counter will be overwritte (if present).
		///</summary>
		[Obsolete()]
		public void AddResult(Type counterType, IList<Candidate> result, uint seats, IEnumerable<VotingSlip> votes) 
        {
            if(result == null)
                throw new ArgumentNullException("Result to cache must not be null", nameof(result));

            var dictKey = new CacheIdentifier { Counter = counterType, Seats = seats, Votes = votes };
            cache[dictKey] = result;
        }

		///<summary>
		/// Cache a specified result.
		/// Previous results for the same counter will be overwritte (if present).
		///</summary>
		public void AddResult(CacheIdentifier cacheIdentifier, IList<Candidate> result)
		{
			if (result == null)
				throw new ArgumentNullException("Result to cache must not be null", nameof(result));

			cache[cacheIdentifier] = result;
		}
	}
}