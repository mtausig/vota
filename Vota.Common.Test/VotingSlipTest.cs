//This file is part of Vota.
//
// Copyright 2019 Mathias Tausig <mathias@tausig.at>
//
//Vota is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Vota is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Vota.  If not, see <https://www.gnu.org/licenses/>.
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Vota.Common.Test 
{
    public class VotingSlipTest
    {   
        Candidate c1 = new Candidate("foo");
        Candidate c2 = new Candidate("bar");
        Candidate c3 = new Candidate("bla");
        Candidate c4 = new Candidate("blubb");

        [Fact]
        public void GetVotesTest()
        {

            Candidate[] myVotes = new Candidate[] {c1, c2};
            VotingSlip votingSlip = new VotingSlip(myVotes);

            Assert.True(votingSlip.Votes.SequenceEqual(myVotes.Select(v => v.Name)));
        }

        [Fact]
        public void GetVotesCannotAlterInstanceTest()
        {
            Candidate[] myVotes = new Candidate[] {c1};
            VotingSlip votingSlip = new VotingSlip(myVotes);
            IList<string> votesInSlip = votingSlip.Votes;
            votesInSlip.Add(c2.Name);
            Assert.True(votingSlip.Votes.SequenceEqual(myVotes.Select(v => v.Name)));
        }

        [Fact]
        public void ConstructorEmptySlipTest()
        {
            Candidate[] myVotes = new Candidate[0];
            // Construction a voting slip without any votes should work
            VotingSlip votingSlip = new VotingSlip(myVotes);
            Assert.False(votingSlip.Votes.Any());
        }

        [Fact]
        public void ConstructorChecksNullValueTest()
        {
            Assert.Throws<ArgumentNullException> ( () => new VotingSlip((string[])null, 1));
        }

        [Fact]
        public void ConstructorDualCandidatesTest()
        {
            Candidate[] myVotes = new Candidate[] {c1, c1};
            Assert.Throws<ArgumentException> ( () => new VotingSlip(myVotes));
        }

         [Fact]
        public void RemoveCandidatesTest()
        {
            Candidate[] myVotes = new Candidate[] {c1, c2, c3};
            VotingSlip votingSlip = new VotingSlip(myVotes);
            VotingSlip reducedVotingSlip = votingSlip.RemoveCandidates(new Candidate[]{c2, c4});
            Assert.True(reducedVotingSlip.Votes.SequenceEqual(new []{c1.Name, c3.Name}));
            Assert.True(votingSlip.Votes.SequenceEqual(new []{c1.Name, c2.Name, c3.Name}), 
                "The original VotingSlip should remain unaltered.");
        }


        [Fact]
        public void RemoveCandidatesChecksNullValueTest()
        {
            VotingSlip votingSlip = new VotingSlip(new Candidate[0]);
            Assert.Throws<ArgumentNullException>( () => votingSlip.RemoveCandidates(null));
        }

        [Fact]
        public void DefaultWeightIsOneTest ()
        {
            VotingSlip votingSlip = new VotingSlip(new Candidate[0]);
            Assert.Equal(1, votingSlip.Weight);
        }

        [Fact]
        public void TransferVoteChecksNullValueTest()
        {
            VotingSlip votingSlip = new VotingSlip(new Candidate[0]);
            Assert.Throws<ArgumentNullException>( () => votingSlip.TransferVote((string)null));
        }

        [Fact]
        public void TransferVoteTopCandidateTest()
        {
            VotingSlip votingSlip = new VotingSlip(c1, c2, c3);
            votingSlip = votingSlip.TransferVote(c1, 0.4);
            Assert.True(votingSlip.Votes.SequenceEqual(new []{c2.Name, c3.Name}));
            Assert.Equal(0.4, votingSlip.Weight);
            votingSlip = votingSlip.TransferVote(c2, 0.5);
            Assert.True(votingSlip.Votes.SequenceEqual(new []{c3.Name}));
            Assert.Equal(0.2, votingSlip.Weight);
        }

        [Fact]
        public void TransferVoteNotTopCandidateTest()
        {
            VotingSlip votingSlip = new VotingSlip(c1, c2, c3);
            votingSlip = votingSlip.TransferVote(c2, 0.4);
            Assert.True(votingSlip.Votes.SequenceEqual(new []{c1.Name, c3.Name}));
            Assert.Equal(1, votingSlip.Weight);
        }


        [Fact]
        public void TransferVoteCandidateNotOnSlipTest()
        {
            VotingSlip votingSlip = new VotingSlip(c1, c2, c3);
            votingSlip = votingSlip.TransferVote(c4, 0.4);
            Assert.True(votingSlip.Votes.SequenceEqual(new []{c1.Name, c2.Name, c3.Name}));
            Assert.Equal(1, votingSlip.Weight);
        }

        [Fact]
        public void EqualsTest()
        {
            VotingSlip slip1 = new VotingSlip(new Candidate[]{c1, c2}, 0.3);
            VotingSlip slip2 = new VotingSlip(new List<Candidate>{c1, c2}, 0.3);
            Assert.Equal(slip1, slip2);
            Assert.Equal(slip1.GetHashCode(), slip2.GetHashCode());
        }

        [Fact]
        public void EqualsDifferentOrderTest()
        {
            VotingSlip slip1 = new VotingSlip(new Candidate[] { c1, c2 }, 0.3);
            VotingSlip slip2 = new VotingSlip(new List<Candidate> { c2, c1 }, 0.3);
            Assert.NotEqual(slip1, slip2);
            Assert.NotEqual(slip1.GetHashCode(), slip2.GetHashCode());
        }

        [Fact]
        public void EqualsTestDifferentCandidatesTest()
        {
            VotingSlip slip1 = new VotingSlip(new Candidate[]{c1, c2, c3}, 0.3);
            VotingSlip slip2 = new VotingSlip(new List<Candidate>{c1, c2}, 0.3);
            Assert.NotEqual(slip1, slip2);
        }

        [Fact]
        public void EqualsDifferentWeightTest()
        {
            VotingSlip slip1 = new VotingSlip(new Candidate[]{c1, c2}, 0.3);
            VotingSlip slip2 = new VotingSlip(new List<Candidate>{c1, c2}, 0.31);
            Assert.NotEqual(slip1, slip2);
        }

        [Fact]
        public void RemoveTopCandidateTest()
        {
            VotingSlip vote = new VotingSlip(c1, c2, c3);

            vote = vote.Remove(c1);
            Assert.Equal(1, vote.Weight);
            Assert.Equal(2, vote.Votes.Count);
            Assert.Equal(c2.Name, vote.Votes[0]);
            Assert.Equal(c3.Name, vote.Votes[1]);
        }      

        [Fact]
        public void RemoveTralingCandidateTest()
        {
            VotingSlip vote = new VotingSlip(c1, c2, c3);

            vote = vote.Remove(c2);
            Assert.Equal(1, vote.Weight);
            Assert.Equal(2, vote.Votes.Count);
            Assert.Equal(c1.Name, vote.Votes[0]);
            Assert.Equal(c3.Name, vote.Votes[1]);

            vote = vote.Remove(c3);
            Assert.Equal(1, vote.Weight);
            Assert.Equal(1, vote.Votes.Count);
            Assert.Equal(c1.Name, vote.Votes[0]);
        }

        [Fact]
        public void RemoveOnlyCandidateTest()
        {
            VotingSlip vote = new VotingSlip(c1);

            vote = vote.Remove(c1);
            Assert.Equal(1, vote.Weight);
            Assert.Equal(0, vote.Votes.Count);
        }

        [Fact]
        public void RemoveCandidateNotOnSlipTest()
        {
            VotingSlip vote = new VotingSlip(c1, c3);

            vote = vote.Remove(c2);
            Assert.Equal(1, vote.Weight);
            Assert.Equal(2, vote.Votes.Count);
            Assert.Equal(c1.Name, vote.Votes[0]);
            Assert.Equal(c3.Name, vote.Votes[1]);
        }

        [Fact]
        public void RestrictCandidatesArgumentCheckTest()
        {
            VotingSlip vote = new VotingSlip();
            Assert.Throws<ArgumentNullException>(() => vote.RestrictCandidates(null));
        }


        [Fact]
        public void RestrictCandidatesNoElectableCandidatesTest()
        {
            VotingSlip vote = new VotingSlip(c1, c2, c3, c4);
            vote = vote.RestrictCandidates(new Candidate[0]);
            Assert.False(vote.Votes.Any());
        }
        

        [Fact]
        public void RestrictCandidatesTest()
        {
            VotingSlip vote = new VotingSlip(0.7, c1, c2, c3);
            vote = vote.RestrictCandidates(new Candidate[]{c2, c3, c4});
            Assert.Equal(vote, new VotingSlip(0.7, c2, c3));
        }


        [Fact]
        public void RestrictCandidatesNoIntersectionTest()
        {
            VotingSlip vote = new VotingSlip(c1, c2);
            vote = vote.RestrictCandidates(new Candidate[]{c3, c4});
            Assert.False(vote.Votes.Any());
        }

        [Fact]
        public void RestrictVotesTest()
        {
            VotingSlip slip = new VotingSlip(0.5, c1, c2, c3) {Name = "foobar"};

            var restricted = slip.RestrictVotes(4);
            Assert.Equal(slip, restricted);
            restricted = slip.RestrictVotes(3);
            Assert.Equal(slip, restricted);
            restricted = slip.RestrictVotes(2);
            Assert.Equal(new VotingSlip(0.5, c1, c2) {Name = slip.Name}, restricted);
            restricted = slip.RestrictVotes(1);
            Assert.Equal(new VotingSlip(0.5, c1) {Name = slip.Name}, restricted);
            
            restricted = slip.RestrictVotes(0);
            Assert.Equal(slip, restricted);
            restricted = slip.RestrictVotes(-4);
            Assert.Equal(slip, restricted);

        }
    }
}