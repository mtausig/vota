using System;
using Xunit;
using Xunit.Abstractions;

namespace Vota.Common.Test
{
    public class TableTest
    {
        private readonly ITestOutputHelper _testOutputHelper;

        public TableTest(ITestOutputHelper testOutputHelper)
        {
            _testOutputHelper = testOutputHelper;
        }

        [Fact()]
        public void ToMarkdownStringTest()
        {
            var table = new Table(new string[] {
                "Col1", "Col2", "", "A longer header",
            });
            table.AddRow(new []{ "1.1", "1.2", "1.3", "1.4"});
            table.AddRow(new []{ "2.1", "2.22222", "2.3"});
            string expectedTable = @"| Col1 | Col2    |     | A longer header |
|------|---------|-----|-----------------|
| 1.1  | 1.2     | 1.3 | 1.4             |
| 2.1  | 2.22222 | 2.3 |                 |
";
            Assert.Equal(expectedTable, table.ToMarkdownString());
            
        }
        
        [Fact()]
        public void ToCsvStringTest()
        {
            var table = new Table(new string[] {
                "Col1", "Col2", "", "A longer header",
            });
            table.AddRow(new []{ "1.1", "1.2", "1.3", "1.4"});
            table.AddRow(new []{ "2.1", "2.22222", "2.3"});
            string expectedCsv = @"Col1;Col2;;A longer header
1.1;1.2;1.3;1.4
2.1;2.22222;2.3;
";
            Assert.Equal(expectedCsv, table.ToCsvString());
            
        }
    }
}