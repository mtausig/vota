//This file is part of Vota.
//
// Copyright 2019 Mathias Tausig <mathias@tausig.at>
//
//Vota is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Vota is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Vota.  If not, see <https://www.gnu.org/licenses/>.
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Vota.Common.Test
{
    public class BallotBoxStatisticsExtensionsTest
    {
        [Fact]
        public void GetRankDistributionTest()
        {
            List<VotingSlip> ballotBox = new List<VotingSlip>() {
                new VotingSlip("C1", "C2", "C3"),
                new VotingSlip("C1", "C2", "C3"),
                new VotingSlip("C1", "C3"),
                new VotingSlip("C3"),
                
            };
            string expectedTable = @"| Name | Gesamtzahl der Stimmen | Davon Stimmen im 1. Rang | Davon Stimmen im 2. Rang | Davon Stimmen im 3. Rang |
|------|------------------------|--------------------------|--------------------------|--------------------------|
| C3   | 4 (100 %)              | 1 (25 %)                 | 1 (25 %)                 | 2 (50 %)                 |
| C1   | 3 (75 %)               | 3 (100 %)                | 0 (0 %)                  | 0 (0 %)                  |
| C2   | 2 (50 %)               | 0 (0 %)                  | 2 (100 %)                | 0 (0 %)                  |
";
            
            Assert.Equal(expectedTable, ballotBox.GetRankDistribution());
        }

        [Fact]
        public void GetQuoteDistributionTest()
        {
            string[] candidates = new[] {
                new Candidate("M1", false).Name,
                new Candidate("M2", false).Name,
                new Candidate("M3", false).Name,
                new Candidate("M4", false).Name,
                new Candidate("F1", true).Name,
                new Candidate("F2", true).Name,
                new Candidate("F3", true).Name,
            };
            List<VotingSlip> ballotBox = new List<VotingSlip>();
            //100 slips, 33,33% quoted (50% top 2)
            ballotBox.AddRange(Enumerable.Repeat(new VotingSlip("M1", "F1", "M2"), 100));
            //100 slips, 42,86% quoted (50% top 4, 0% top 2)
            ballotBox.AddRange(Enumerable.Repeat(new VotingSlip("M1", "M2", "F1", "F2", "M3", "F3", "M4"), 100));
            //50 slips, 60% quoted (75% top 4, 100% top 2)
            ballotBox.AddRange(Enumerable.Repeat(new VotingSlip("F1", "F2", "F3", "M1", "M2"), 50));
            //50 slips, 100% quoted
            ballotBox.AddRange(Enumerable.Repeat(new VotingSlip("F3"), 50));
            //Invalid slip with no votes; should not influence the result
            ballotBox.Add(new VotingSlip());

            string expectedTable = @";<=20%;20%-40%;40%-60%;60%-80%;>80%
Ganzer Stimmzettel;0;100;150;0;50
Ganzer Stimmzettel (%);0%;33.33%;50%;0%;16.67%
Top 2;100;0;100;0;100
Top 2 (%);33.33%;0%;33.33%;0%;33.33%
Top 4;0;100;100;50;50
Top 4 (%);0%;33.33%;33.33%;16.67%;16.67%
";
            var table = ballotBox.GetQuoteDistribution(4);
            Assert.Equal(expectedTable, table.ToCsvString());
        }
    }
}
