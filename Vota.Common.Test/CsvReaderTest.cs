//This file is part of Vota.
//
// Copyright 2019 Mathias Tausig <mathias@tausig.at>
//
//Vota is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Vota is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Vota.  If not, see <https://www.gnu.org/licenses/>.
using System;
using System.IO;
using System.Linq;
using Xunit;

namespace Vota.Common.Test
{
    public class CsvReaderTest
    {
        static Candidate c1 = new Candidate("Mr. foo");
        static Candidate c2 = new Candidate("Mr. bar");
        static Candidate c3 = new Candidate("Mrs. foo");
        static Candidate c4 = new Candidate("Mrs. bar");
        static Candidate[] allCandidates = new[] {c1, c2, c3, c4};


        [Fact]
        public void ReadCsvFileTest()
        {
            //We will just test, if the parsed content of the file equals that of the parsed string
            string csv = @"Mr. foo;Mrs. bar
Mr. bar;Mr. foo";
            string tmpFile = Path.GetTempFileName();
            try {
                File.WriteAllText(tmpFile, csv);
                var parsedFromFile = CsvReader.ReadCsvFile(tmpFile);
                var parsedFromString = CsvReader.ParseCsvList(csv);
                Assert.True(parsedFromFile.SequenceEqual(parsedFromString));
            }
            finally {
                File.Delete(tmpFile);
            }
        }

        [Fact]
        public void ParseCsvTest()
        {
            //We will just test, if the parsed content of the file equals that of the parsed string
            string csv = @"Mr. foo;Mrs. bar
Mr. bar;Mr. foo";
            var parsed = CsvReader.ParseCsvList(csv);
            Assert.Equal(2, parsed.Count);
            Assert.Equal(parsed[0], new VotingSlip(c1, c4));
            Assert.Equal(parsed[1], new VotingSlip(c2, c1));
        }

        [Fact]
        public void ParseCsTrimEmptyLinesTest()
        {
            //We will just test, if the parsed content of the file equals that of the parsed string
            string csv = @"
Mr. foo;Mrs. bar
Mr. bar;Mr. foo

    ";
            var parsed = CsvReader.ParseCsvList(csv, null, true);
            Assert.Equal(3, parsed.Count);
            Assert.False(parsed[0].Votes.Any());
            Assert.Equal(parsed[1], new VotingSlip(c1, c4));
            Assert.Equal(parsed[2], new VotingSlip(c2, c1));
        }

        [Fact]
        public void ParseCsvWithAvailableCandidatesTest()
        {
            //We will just test, if the parsed content of the file equals that of the parsed string
            string csv = @"Mr. foo;Mrs. bar
Mr. bar;Mr. foo";
            Candidate[] availableCandidates = new Candidate[] {c1, c2};
            var parsed = CsvReader.ParseCsvList(csv, availableCandidates);
            Assert.Equal(2, parsed.Count);
            Assert.Equal(new VotingSlip(c1), parsed[0]);
            Assert.Equal(new VotingSlip(c2, c1), parsed[1]);
        }

        [Fact]
        public void ParseCsvTrimNamesTest()
        {
            //We will just test, if the parsed content of the file equals that of the parsed string
            string csv = @"Mr. foo;  Mrs. bar  
      Mr. bar;Mr. foo ";
            var parsed = CsvReader.ParseCsvList(csv);
            Assert.Equal(2, parsed.Count);
            Assert.Equal(parsed[0], new VotingSlip(c1, c4));
            Assert.Equal(parsed[1], new VotingSlip(c2, c1));
        }

        [Fact]
        public void ParseCsvWithEmptyLineTest()
        {
            //We will just test, if the parsed content of the file equals that of the parsed string
            string csv = @"Mr. foo;Mrs. bar

Mr. bar;Mr. foo";
            var parsed = CsvReader.ParseCsvList(csv);
            Assert.Equal(3, parsed.Count);
            Assert.Equal(parsed[0], new VotingSlip(c1, c4));
            Assert.Equal(0, parsed[1].Votes.Count);
            Assert.Equal(parsed[2], new VotingSlip(c2, c1));
        }

        [Fact]
        public void ParseCsvMultipleEmptyNamesIgnoredTest()
        {
            //We will just test, if the parsed content of the file equals that of the parsed string
            string csv = @"Mr. foo;Mrs. bar;;;";
            var parsed = CsvReader.ParseCsvList(csv);
            Assert.Equal(1, parsed.Count);
            Assert.Equal(parsed[0], new VotingSlip(c1, c4));
        }

        [Fact]
        public void ParseCsvWithOnlyEmptyLineTest()
        {
            //We will just test, if the parsed content of the file equals that of the parsed string
            string csv = @"
";
            var parsed = CsvReader.ParseCsvList(csv);
            Assert.Equal(2, parsed.Count);
            Assert.Equal(0, parsed[0].Votes.Count);
            Assert.Equal(0, parsed[1].Votes.Count);
        }

        [Fact]
        public void ParseCandidatesTest()
        {
            string csv = @"A
B;true
C;1
D;

E;foobar

";
            var candidates = CsvReader.ParseCandidates(csv);

            Assert.Equal(5, candidates.Count);
            Assert.Equal("A", candidates[0].Name);
            Assert.False(candidates[0].EligibleForQuote);
            Assert.Equal("B", candidates[1].Name);
            Assert.True(candidates[1].EligibleForQuote);
            Assert.Equal("C", candidates[2].Name);
            Assert.True(candidates[2].EligibleForQuote);
            Assert.Equal("D", candidates[3].Name);
            Assert.False(candidates[3].EligibleForQuote);
            Assert.Equal("E", candidates[4].Name);
            Assert.False(candidates[4].EligibleForQuote);
        }


        [Fact]
        public void ParseCsvSpreadsheetTest()
        {
            string csv = @"  ;Mr. foo; Mr. bar ; Mrs. foo; Mrs. bar
A-01; 1          ;     2      ; 4          ; 3
    ; 2          ;     1      ;            ; 3
A-02;            ;            ;            ;  
A-03; 1          ;            ;            ; 2
;
A-04;;1

";
            var parsed = CsvReader.ParseCsvSpreadsheet(csv, allCandidates);
            Assert.Equal(6, parsed.Count());
            
            Assert.Equal("A-01", parsed[0].Name);
            Assert.True(parsed[0].Votes.SequenceEqual(new []{c1.Name,c2.Name,c4.Name,c3.Name}));
            
            Assert.Equal("", parsed[1].Name);
            Assert.True(parsed[1].Votes.SequenceEqual(new []{c2.Name,c1.Name,c4.Name}));
            
            Assert.Equal("A-02", parsed[2].Name);
            Assert.Equal(0, parsed[2].Votes.Count);
            
            Assert.Equal("A-03", parsed[3].Name);
            Assert.True(parsed[3].Votes.SequenceEqual(new []{c1.Name,c4.Name}));
            
            Assert.Equal("", parsed[4].Name);
            Assert.Equal(0, parsed[4].Votes.Count);
            
            Assert.Equal("A-04", parsed[5].Name);
            Assert.True(parsed[5].Votes.SequenceEqual(new []{c2.Name}));
        }

        [Fact]
        public void ParseCsvSpreadsheetBadNameTest()
        {
            string csv = @"  ;Mr. foo; Mr. bar ; Mrs. foo; Mrs. bar;Mr. X
A-01; 1          ;     2      ; 4          ; 3
    ; 2          ;     1      ;            ; 3
A-02;            ;            ;            ;  
A-03; 1          ;            ;            ; 2

A-04;;1
";
            Assert.Throws<ArgumentException>( () => CsvReader.ParseCsvSpreadsheet(csv, allCandidates));
            // Check that parsing without prespecified candidates works as expected
            Assert.Equal(5, CsvReader.ParseCsvSpreadsheet(csv, null).Count());
        }

        [Fact]
        public void ParseCsvSpreadsheetTrimEmptyTest()
        {
            string csv = @"  ;Mr. foo; Mr. bar ; Mrs. foo; Mrs. bar
A-01; 1          ;     2      ; 4          ; 3
A-02;            ;            ;            ;  
A-03; 1          ;            ;            ; 2
A-04;;1

";
            var parsed = CsvReader.ParseCsvSpreadsheet(csv, allCandidates, true);
            Assert.Equal(4, parsed.Count());
        }


        [Fact]
        public void ParseCsvSpreadsheetArgumentChecksTest()
        {
            Assert.Throws<ArgumentException>( () => CsvReader.ParseCsvSpreadsheet("", allCandidates));
            Assert.Throws<ArgumentException>( () => CsvReader.ParseCsvSpreadsheet("  ", allCandidates));
            Assert.Throws<ArgumentNullException>( () => CsvReader.ParseCsvSpreadsheet(null, allCandidates));
        }
        
        [Fact]
        public void ParseCsvSpreadsheetTooManyFieldsTest()
        {
            string csv = @"  ;Mr. foo; Mr. bar ; Mrs. foo; Mrs. bar;
A-01; 1          ;     2      ; 4          ; 3;5
    ; 2          ;     1      ;            ; 3
A-02;            ;            ;            ;  
A-03; 1          ;            ;            ; 2

A-04;;1
";
            Assert.Throws<ArgumentException>( () => CsvReader.ParseCsvSpreadsheet(csv, allCandidates));
        }
        
        [Fact]
        public void ParseCsvSpreadsheetDuplicateSlipNamesTest()
        {
            string csv = @"  ;Mr. foo; Mr. bar ; Mrs. foo; Mrs. bar;
A-01; 1          ;     2      ; 4          ; 3
    ; 2          ;     1      ;            ; 3
A-02;            ;            ;            ;  
A-01; 1          ;            ;            ; 2

A-04;;1
";
            Assert.Throws<ArgumentException>( () => CsvReader.ParseCsvSpreadsheet(csv, allCandidates));
        }

        [Fact]
        public void ParseCsvSpreadsheetBadRanksTest()
        {
            // skipping a rank
            string csv = @"  ;Mr. foo; Mr. bar ; Mrs. foo; Mrs. bar
A-01; 1          ;     2      ; 4          ; 6";
            Assert.Throws<ArgumentException>(() => CsvReader.ParseCsvSpreadsheet(csv, allCandidates));

            // starting not at 1
            csv = @"  ;Mr. foo; Mr. bar ; Mrs. foo; Mrs. bar
A-01; 2          ;     3      ; 4          ; 5";
            Assert.Throws<ArgumentException>(() => CsvReader.ParseCsvSpreadsheet(csv, allCandidates));
            csv = @"  ;Mr. foo; Mr. bar ; Mrs. foo; Mrs. bar
A-01; 0          ;     1      ; 2          ; 3";
            Assert.Throws<ArgumentException>(() => CsvReader.ParseCsvSpreadsheet(csv, allCandidates));

            // Duplicate rank
            csv = @"  ;Mr. foo; Mr. bar ; Mrs. foo; Mrs. bar
A-01; 1          ;     2      ; 2          ; 3";
            Assert.Throws<ArgumentException>(() => CsvReader.ParseCsvSpreadsheet(csv, allCandidates));
            csv = @"  ;Mr. foo; Mr. bar ; Mrs. foo; Mrs. bar
A-01; 1          ;     2      ; 2          ; 4";
            Assert.Throws<ArgumentException>(() => CsvReader.ParseCsvSpreadsheet(csv, allCandidates));
            csv = @"  ;Mr. foo; Mr. bar ; Mrs. foo; Mrs. bar
A-01; 1          ;     1      ;           ; ";
            Assert.Throws<ArgumentException>(() => CsvReader.ParseCsvSpreadsheet(csv, allCandidates));

            // Non positive integer values
            csv = @"  ;Mr. foo; Mr. bar ; Mrs. foo; Mrs. bar
A-01; 1.0          ;           ;           ; ";
            Assert.Throws<ArgumentException>(() => CsvReader.ParseCsvSpreadsheet(csv, allCandidates));
            csv = @"  ;Mr. foo; Mr. bar ; Mrs. foo; Mrs. bar
A-01; 1          ; 2.1          ; 3          ; ";
            Assert.Throws<ArgumentException>(() => CsvReader.ParseCsvSpreadsheet(csv, allCandidates));
            csv = @"  ;Mr. foo; Mr. bar ; Mrs. foo; Mrs. bar
A-01; -1          ; 2          ;           ; ";
            Assert.Throws<ArgumentException>(() => CsvReader.ParseCsvSpreadsheet(csv, allCandidates));
            csv = @"  ;Mr. foo; Mr. bar ; Mrs. foo; Mrs. bar
A-01; 1          ; 2         ; XXX          ; ";
            Assert.Throws<ArgumentException>(() => CsvReader.ParseCsvSpreadsheet(csv, allCandidates));
        }

        [Fact]
        public void TrimEmptyLinesTest()
        {
            string text = @"Line 1
Line 2

Line 4


        ";
            var lines = text.Split("\n");
            lines = CsvReader.TrimEmptyLines(lines).ToArray();
            Assert.Equal(4, lines.Length);
            Assert.Equal("Line 4", lines[3]);
        }

    }
}