//This file is part of Vota.
//
// Copyright 2019 Mathias Tausig <mathias@tausig.at>
//
//Vota is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Vota is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Vota.  If not, see <https://www.gnu.org/licenses/>.
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Vota.Common.Test
{
    public class BallotBoxExtensionsTest
    {
        string c1 = "foo";
        string c2 = "bar";
        string c3 = "bla";
        string c4 = "blubb";

        [Fact]
        public void TransferVoteChecksNullValueTest()
        {
            List<VotingSlip> ballotBox = new List<VotingSlip>();
            Assert.Throws<ArgumentNullException>( () => ballotBox.TransferVotes(null));
        }

        [Fact]
        public void TransferVotesTest()
        {
            // We will create a few voting slips with all scenarios already tested in VotingSlipTest, and then just check if the transfer equals that of the transfer done individually
            VotingSlip slipWithTopCandidate = new VotingSlip(c1, c2, c3);
            VotingSlip slipWithCandidateNotTop = new VotingSlip(c2, c1, c4);
            VotingSlip slipWithCandidateNotPresent = new VotingSlip(c4);

            IList<VotingSlip> ballotBox = new List<VotingSlip> {
                slipWithTopCandidate, slipWithCandidateNotTop, slipWithCandidateNotPresent,
            };
            ballotBox = ballotBox.TransferVotes(c1, 0.4);
            Assert.Equal(ballotBox[0], slipWithTopCandidate.TransferVote(c1, 0.4));
            Assert.Equal(ballotBox[1], slipWithCandidateNotTop.TransferVote(c1, 0.4));
            Assert.Equal(ballotBox[2], slipWithCandidateNotPresent.TransferVote(c1, 0.4));
        }

        [Fact]
        public void GetCandidatesArgumentNullCheckTest()
        {
            VotingSlip[] votes = null;
            Assert.Throws<ArgumentNullException>( () => votes.GetCandidates());
        }

        [Fact]
        public void GetCandidatesTest()
        {
            VotingSlip[] votes = new VotingSlip[]{
                new VotingSlip(c1, c2),
                new VotingSlip(c2),
                new VotingSlip(c3, c1, c2),
                new VotingSlip(),
                new VotingSlip(c1, c3),
            };
            var candidates = votes.GetCandidates();
            Assert.Equal(3, candidates.Count);
            Assert.True(candidates.Contains(new Candidate(c1)), String.Format("Candidates should contain {0}.", c1));
            Assert.True(candidates.Contains(new Candidate(c2)), String.Format("Candidates should contain {0}.", c2));
            Assert.True(candidates.Contains(new Candidate(c3)), String.Format("Candidates should contain {0}.", c3));
        }

        [Fact]
        public void GetVotesPerCandidateArgumentNullCheckTest()
        {
             VotingSlip[] votes = null;
            Assert.Throws<ArgumentNullException>( () => votes.GetVotesPerCandidate());
        }

        [Fact]
        public void GetVotesPerCandidateDefaultWeightTest()
        {
            VotingSlip[] votes = new VotingSlip[]{
                new VotingSlip(c1, c2),
                new VotingSlip(c2),
                new VotingSlip(c3, c1, c2),
                new VotingSlip(),
                new VotingSlip(c1, c3, c4),
            };
            var votesPerCandidate = votes.GetVotesPerCandidate();
            Assert.Equal(4, votesPerCandidate.Count);
            Assert.Equal(2, votesPerCandidate[new Candidate(c1)]);
            Assert.Equal(1, votesPerCandidate[new Candidate(c2)]);
            Assert.Equal(1, votesPerCandidate[new Candidate(c3)]);
            Assert.Equal(0, votesPerCandidate[new Candidate(c4)]);
        }

        [Fact]
        public void GetVotesPerCandidateDifferentWeightTest()
        {
            VotingSlip[] votes = new VotingSlip[]{
                new VotingSlip(0.5, c1, c2),
                new VotingSlip(c2),
                new VotingSlip(0.1, c3, c1, c2),
                new VotingSlip(),
                new VotingSlip(0.7, c1, c3, c4),
            };
            var votesPerCandidate = votes.GetVotesPerCandidate();
            Assert.Equal(4, votesPerCandidate.Count);
            Assert.Equal(1.2, votesPerCandidate[new Candidate(c1)]);
            Assert.Equal(1, votesPerCandidate[new Candidate(c2)]);
            Assert.Equal(0.1, votesPerCandidate[new Candidate(c3)]);
            Assert.Equal(0, votesPerCandidate[new Candidate(c4)]);
        }

         [Fact]
        public void GetVotesPerCandidateVotesFromCsvTest()
        {
            var votes = CsvReader.ParseCsvList(@"John;Paul
John;Paul
John;Paul
John;Ringo
John;Ringo
Ringo; Paul; John
Ringo; Paul; John
Ringo; Paul; John
Ringo; Paul; John");
            var votesPerCandidate = votes.GetVotesPerCandidate();
            Assert.Equal(5, votesPerCandidate[new Candidate("John") ]);
            Assert.Equal(4, votesPerCandidate[new Candidate("Ringo") ]);
            Assert.Equal(0, votesPerCandidate[new Candidate("Paul") ]);
        }


        [Fact]
        public void RemoveArgumentNullCheckTest()
        {
            VotingSlip[] votes = null;
            Assert.Throws<ArgumentNullException> ( () => BallotBoxExtensions.Remove(votes, new Candidate("foo")));
            votes = new VotingSlip[0];
            Assert.Throws<ArgumentNullException> ( () => votes.Remove((Candidate)null));
        }

        [Fact]
        public void RemoveTest()
        {
            // We will create a few voting slips with all scenarios already tested in VotingSlipTest, and then just check if the transfer equals that of the transfer done individually
            VotingSlip slipWithTopCandidate = new VotingSlip(c1, c2, c3);
            VotingSlip slipWithCandidateNotTop = new VotingSlip(c2, c1, c4);
            VotingSlip slipWithCandidateNotPresent = new VotingSlip(c4);

            IList<VotingSlip> ballotBox = new List<VotingSlip> {
                new VotingSlip(c1, c2, c3),
                new VotingSlip(c2),
                new VotingSlip(c2, c1),
                new VotingSlip(c1, c3, c2),
            };

            ballotBox = ballotBox.Remove(c2);
            Assert.Equal(4, ballotBox.Count);
            //Check that the weight of the votes hasn't changed
            foreach(VotingSlip vote in ballotBox)
                Assert.Equal(1, vote.Weight);

            Assert.Equal(2, ballotBox[0].Votes.Count);
            Assert.Equal(c1, ballotBox[0].Votes[0]);
            Assert.Equal(c3, ballotBox[0].Votes[1]);

            Assert.Equal(0, ballotBox[1].Votes.Count);

            Assert.Equal(1, ballotBox[2].Votes.Count);
            Assert.Equal(c1, ballotBox[2].Votes[0]);

            Assert.Equal(2, ballotBox[3].Votes.Count);
            Assert.Equal(c1, ballotBox[3].Votes[0]);
            Assert.Equal(c3, ballotBox[3].Votes[1]);
        }

        [Fact]
        public void RestrictCandidatesTest()
        {
            Candidate[] electableCandidates = new Candidate[] {new Candidate(c1), new Candidate(c2)};

            VotingSlip slipWithOnlyElectableCandidates = new VotingSlip(c1, c2);
            VotingSlip slipWithOnlyOneElectableCandidate = new VotingSlip(c1);
            VotingSlip slipWithElectableAndNotElectableCandidates = new VotingSlip(c2, c4);
            VotingSlip slipWithNoElectableCandidates = new VotingSlip(c4, c3);
            VotingSlip slipWithWeight = new VotingSlip(0.33, c1, c3);

            IList<VotingSlip> ballotBox = new List<VotingSlip> {
                slipWithOnlyElectableCandidates,
                slipWithOnlyOneElectableCandidate,
                slipWithElectableAndNotElectableCandidates,
                slipWithNoElectableCandidates,
                slipWithWeight,
            };

            IList<VotingSlip> expectedRestrictedBallotBox = new List<VotingSlip> {
                new VotingSlip(c1, c2),
                new VotingSlip(c1),
                new VotingSlip(c2),
                new VotingSlip(),
                new VotingSlip(0.33, c1),
            };


            ballotBox = ballotBox.RestrictCandidates(electableCandidates);
            Assert.True(ballotBox.SequenceEqual(expectedRestrictedBallotBox));
        }

        [Fact]
        public void RestricCandidatesDropEmptyVotesTest()
        {
            var votes = CsvReader.ParseCsvList(@"John
John
John
John

George
George

Paul
Paul
Paul

Ringo
Ringo
Ringo

foo
bar
bla

");
            var candidates = new Candidate[]
           {
                new Candidate("John"), new Candidate("Paul"),
                new Candidate("George"), new Candidate("Ringo"),
           };
            Assert.Equal(12, votes.RestrictCandidates(candidates, true).Count);
        }


        [Fact]
        public void SelectRandomCandidateTest()
        {
            var candidates = new Candidate[]{new Candidate(c1), new Candidate(c2), new Candidate(c3), };


            // Lets run the test a couple of times to check if all candidates are selected from time to time
            Dictionary<Candidate, bool> haveWon = new Dictionary<Candidate, bool>();
            // We stop at 1024 runs, it it extremely unlikely (1/2^{1024}) that we will get a false negative
            for (int i = 0; i < 1024; ++i) {
                var selected = candidates.SelectRandomCandidate();
                haveWon[selected] = true;
                if (haveWon.Count == candidates.Length)
                    break;
            }

            Assert.Equal(candidates.Length, haveWon.Count);
        }

		[Fact]
		public void RankCandidatesOnBestSelectionsTest()
		{
			IList<VotingSlip> ballotBox = new List<VotingSlip> {
				new VotingSlip(c1, c2, c3),
				new VotingSlip(c1),
				new VotingSlip(c2, c1),
				new VotingSlip(c3, c2),
				new VotingSlip(c4),
			};
			var candidates = new Candidate[]{
				new Candidate(c2), new Candidate(c4), new Candidate(c3), new Candidate(c1),
			};

			var candidatesOrderedByRank = ballotBox.RankCandidatesOnBestSelections(candidates);
			Assert.Equal(4, candidatesOrderedByRank.Count);
			Assert.Equal(c1, candidatesOrderedByRank[0].Name);
			Assert.Equal(c2, candidatesOrderedByRank[1].Name);
			Assert.Equal(c3, candidatesOrderedByRank[2].Name);
			Assert.Equal(c4, candidatesOrderedByRank[3].Name);

			candidates = new Candidate[]{
				new Candidate(c4), new Candidate(c2)
			};

			candidatesOrderedByRank = ballotBox.RankCandidatesOnBestSelections(candidates);
			Assert.Equal(2, candidatesOrderedByRank.Count);
			Assert.Equal(c2, candidatesOrderedByRank[0].Name);
			Assert.Equal(c4, candidatesOrderedByRank[1].Name);
		}


	}
}
