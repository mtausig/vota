//This file is part of Vota.
//
// Copyright 2019 Mathias Tausig <mathias@tausig.at>
//
//Vota is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Vota is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Vota.  If not, see <https://www.gnu.org/licenses/>.
using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using ServiceStack;
using ServiceStack.Data;
using ServiceStack.OrmLite;
using ServiceStack.Testing;
using Sing;
using Vota.Common;
using Vota.ServiceInterface;
using Vota.ServiceModel;
using Vota.ServiceModel.Types;
using Candidate = Vota.ServiceModel.Types.Candidate;
using VotingSlip = Vota.ServiceModel.Types.VotingSlip;

namespace Vota.Tests.ServiceInterface
{
    public class ResultServiceTest
    {
        private ServiceStackHost appHost;
        
        private static Election[] testDataElections = {
            new Election() {
                Name = "Party board", Type = ElectionType.QuotedSTV,
                MinCandidatesElected = 0, MaxCandidatesElected = 2
            },
            new Election() {
                Name = "Parliamentary candidates", Type = ElectionType.RankedSTV,
                MinCandidatesElected = 0, MaxCandidatesElected = 4
            },
            new Election() {
                Name = "Chairman", Type = ElectionType.STV,
                MinCandidatesElected = 1, MaxCandidatesElected = 1
            },
        };

        private static Candidate[] testDataCandidates = {
            new Candidate() {Name = "Mr. A", EligibleForQuote = false},
            new Candidate() {Name = "Mrs. B", EligibleForQuote = true},
            new Candidate() {Name = "Mr. C", EligibleForQuote = false},
            new Candidate() {Name = "Mrs. D", EligibleForQuote = true},
        };

        private static Dictionary<Election, Candidate[]> testDataRunning = new Dictionary<Election, Candidate[]>() {
            {
                testDataElections[0],
                new Candidate[] {
                    testDataCandidates[0], testDataCandidates[1], testDataCandidates[2], testDataCandidates[3]
                }
            },
            {
                testDataElections[1],
                new Candidate[] {
                    testDataCandidates[0], testDataCandidates[1], testDataCandidates[2], testDataCandidates[3]
                }
            }, 
            {
                testDataElections[2],
                new Candidate[] {
                    testDataCandidates[0], testDataCandidates[1], testDataCandidates[2], testDataCandidates[3]
                }
            },
        };

        private static Dictionary<Election, string[][]> testDataVotes = new Dictionary<Election, string[][]>() {
            {
                testDataElections[0],
                new string[][]{ new []{ "Mr. A", "Mr. C"}, new []{"Mr. A", "Mrs. D"}, new []{"Mrs. D"}, 
                    new []{"Mr. A"}, new []{"Mrs. D", "Mrs. B"} }
            },
            {
                testDataElections[1],
                new string[][]{ 
                    new []{ "Mr. A", "Mr. C", "Mrs. B", "Mrs. D"}, new []{"Mr. A", "Mrs. D","Mr. C", "Mrs. B"}, 
                    new []{"Mrs. D", "Mr. A", "Mrs. B", "Mr. C"}, new []{"Mr. A", "Mrs. B", "Mr. C", "Mrs. D"}, 
                    new []{"Mrs. D", "Mrs. B", "Mr. A", "Mr. C"} }
            },
            {
                testDataElections[2],
                
                new string[][]{ new []{ "Mr. A", "Mr. C"}, new []{"Mrs. C", "Mrs. D", "Mr. A"}, new []{"Mrs. D"}, 
                    new []{"Mr. A"}, new []{"Mrs. D", "Mrs. B"} }
            },
        };

        private static Dictionary<Election, ResultResponse> testDataExpectedResults = new Dictionary<Election, ResultResponse>() {
            {
              testDataElections[0],
              new ResultResponse() {Ordered = false, Elected = new List<string>(){"Mr. A", "Mrs. D"}, Protocol = "TODO"}
            },
            {
                testDataElections[1],
                new ResultResponse() {Ordered = true, Elected = new List<string>(){"Mr. A", "Mrs. D"}, Protocol = "TODO"}
            },
            {
                testDataElections[2],
                new ResultResponse() {Ordered = false, Elected = new List<string>(){"Mrs. D"}, Protocol = "TODO"}
            },
        };


        [OneTimeSetUp]
        public void Setup()
        {
            appHost = new BasicAppHost().Init();
            var container = appHost.Container;
            container.AddTransient<ResultService>();
            container.AddTransient<ElectionsCandidatesService>();
            container.AddTransient<ElectionsDetailsService>();
            container.AddTransient<VotingSlipsService>();

            container.Register<IDbConnectionFactory>(
                new OrmLiteConnectionFactory(":memory:", SqliteDialect.Provider));
            
            // Populate the database
            using (var db = container.Resolve<IDbConnectionFactory>().OpenDbConnection()) {
                // Create tables if they do not exist
                db.CreateTableIfNotExists(typeof(Election));
                db.CreateTableIfNotExists(typeof(Candidate));
                db.CreateTableIfNotExists(typeof(CandidateInElection));
                db.CreateTableIfNotExists(typeof(VotingSlip));
                
                // Add Test data into the db
                foreach(var candidate in testDataCandidates)
                    db.Insert(candidate);
                foreach (var election in testDataElections) {
                    db.Insert(election);
                    foreach (var candidate in testDataRunning[election])
                        db.Insert(new CandidateInElection() {CandidateId = candidate.Id, ElectionId = election.Id});
                    foreach (var votes in testDataVotes[election]) 
                        db.Insert(new VotingSlip() {ElectionId = election.Id, Name = Guid.NewGuid().ToString(), Votes = votes.ToList()});
                }
            }
        }

        [OneTimeTearDown]
        public void OneTimeTearDown() => appHost.Dispose();

       [Test]
        public void GetTest()
        {
            var service = appHost.Container.Resolve<ResultService>();

            foreach (var election in testDataElections) {
                var result = service.Get(new ResultRequest() {Id = election.Id});
                var expectedResult = testDataExpectedResults[election];
                Assert.That(result.Ordered, Is.EqualTo(expectedResult.Ordered), 
                    String.Format("Vote for election '{0}' has the wrong ordered flag.", election.Name));
                Assert.That(result.Elected.Count, Is.EqualTo(expectedResult.Elected.Count),
                    String.Format("Vote for election '{0}' has the wrong number of elected people.", election.Name));
                if (result.Ordered)
                    Assert.True(result.Elected.SequenceEqual(expectedResult.Elected),
                        String.Format("List for election '{0}' is not as expected.", election.Name));
                else {
                    foreach (var elected in result.Elected) {
                        Assert.True(expectedResult.Elected.Contains(elected),
                            String.Format("In election '{0}' the candidate '{1}' should not be elected.", election.Name, elected));
                    }
                }
             //TODO: Maybe we should add a check for a protocol?   
            //Assert.That(result.Protocol, Is.EqualTo(expectedResult.Protocol));
            }
        }
    }
}
