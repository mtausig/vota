//This file is part of Vota.
//
// Copyright 2019 Mathias Tausig <mathias@tausig.at>
//
//Vota is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Vota is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Vota.  If not, see <https://www.gnu.org/licenses/>.
using System.Collections.Generic;
using NUnit.Framework;
using ServiceStack;
using ServiceStack.Data;
using ServiceStack.OrmLite;
using ServiceStack.Testing;
using Sing;
using Vota.Common;
using Vota.ServiceInterface;
using Vota.ServiceModel;
using Vota.ServiceModel.Types;
using Candidate = Vota.ServiceModel.Types.Candidate;

namespace Vota.Tests.ServiceInterface
{
    public class ElectionsCandidatesServiceTest
    {
        private ServiceStackHost appHost;
        
        private static Election[] testDataElections = {
            new Election() {
                Name = "Party board", Type = ElectionType.QuotedSTV,
                MinCandidatesElected = 0, MaxCandidatesElected = 6
            },
            new Election() {
                Name = "Parliamentary candidates", Type = ElectionType.RankedSTV,
                MinCandidatesElected = 4, MaxCandidatesElected = 10
            },
        };

        private static Dictionary<Election, Candidate[]> testDataCandidates = new Dictionary<Election, Candidate[]>() {
            {
                testDataElections[0],
                new Candidate[] {
                    new Candidate() {Name = "Mr. A", EligibleForQuote = false}, 
                    new Candidate() {Name = "Mr. B",EligibleForQuote = true}
                }
            },
            {
                testDataElections[1],
                new Candidate[0]
            }, 
        };
     
        [OneTimeSetUp]
        public void Setup()
        {
            appHost = new BasicAppHost().Init();
            var container = appHost.Container;
            container.AddTransient<ElectionsCandidatesService>();

            container.Register<IDbConnectionFactory>(
                new OrmLiteConnectionFactory(":memory:", SqliteDialect.Provider));
            
            // Populate the database
            using (var db = container.Resolve<IDbConnectionFactory>().OpenDbConnection()) {
                // Create tables if they do not exist
                db.CreateTableIfNotExists(typeof(Election));
                db.CreateTableIfNotExists(typeof(Candidate));
                db.CreateTableIfNotExists(typeof(CandidateInElection));
                
                // Add Test data into the db
                foreach(var row in testDataElections)
                    db.Insert(row);
                foreach(var candidates in testDataCandidates.Values)
                    foreach(var candidate in candidates)
                        db.Insert(candidate);
                foreach (var election in testDataCandidates.Keys)
                    foreach (var candidate in testDataCandidates[election])
                        db.Insert(new CandidateInElection() {CandidateId = candidate.Id, ElectionId = election.Id});
                
            }
        }

        [OneTimeTearDown]
        public void OneTimeTearDown() => appHost.Dispose();

        [Test]
        public void GetTest()
        {
            var service = appHost.Container.Resolve<ElectionsCandidatesService>();

            foreach(var election in testDataElections)
            {
                var response = (ElectionsCandidatesResponse)service.Get(new ElectionsCandidatesRequest() {Id = election.Id});
                Assert.That(response.Candidates.Count, Is.EqualTo(testDataCandidates[election].Length));
                foreach (var candidateDb in testDataCandidates[election]) {
                    var candidate = candidateDb.ConvertTo<ElectionsCandidatesResponse.Candidate>();
                    Assert.True(response.Candidates.Contains(candidate),
                        "Candidate '{0}' was not found in the election '{1}' as expected", candidate, election.Name);
                    
                }
            }
        }
    }
}
