//This file is part of Vota.
//
// Copyright 2019 Mathias Tausig <mathias@tausig.at>
//
//Vota is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Vota is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Vota.  If not, see <https://www.gnu.org/licenses/>.
using System;
using System.Collections.Generic;

using NUnit.Framework;
using ServiceStack;
using ServiceStack.Data;
using ServiceStack.OrmLite;
using ServiceStack.Testing;
using Sing;
using Vota.Common;
using Vota.ServiceInterface;
using Vota.ServiceModel;
using Vota.ServiceModel.Types;
using Candidate = Vota.ServiceModel.Types.Candidate;
using VotingSlip = Vota.ServiceModel.Types.VotingSlip;

namespace Vota.Tests.ServiceInterface
{
    public class VotingSlipsServiceTest
    {
        private ServiceStackHost appHost;
        
        private static Election[] testDataElections = {
            new Election() {
                Name = "Party board", Type = ElectionType.QuotedSTV,
                MinCandidatesElected = 0, MaxCandidatesElected = 6
            },
            new Election() {
                Name = "Parliamentary candidates", Type = ElectionType.RankedSTV,
                MinCandidatesElected = 4, MaxCandidatesElected = 10
            },
        };

        private static Candidate[] testDataCandidates = {
            new Candidate() {Name = "Mr. A", EligibleForQuote = false},
            new Candidate() {Name = "Mrs. B", EligibleForQuote = true},
        };

        private static Dictionary<Election, Candidate[]> testDataRunning = new Dictionary<Election, Candidate[]>() {
            {
                testDataElections[0],
                new Candidate[] {
                    testDataCandidates[0], testDataCandidates[1]
                }
            },
            {
                testDataElections[1],
                new Candidate[] {
                    testDataCandidates[0],
                }
            }, 
        };
     
        [OneTimeSetUp]
        public void Setup()
        {
            appHost = new BasicAppHost().Init();
            var container = appHost.Container;
            container.AddTransient<VotingSlipsService>();
            container.AddTransient<ElectionsCandidatesService>();

            container.Register<IDbConnectionFactory>(
                new OrmLiteConnectionFactory(":memory:", SqliteDialect.Provider));
            
            // Populate the database
            using (var db = container.Resolve<IDbConnectionFactory>().OpenDbConnection()) {
                // Create tables if they do not exist
                db.CreateTableIfNotExists(typeof(Election));
                db.CreateTableIfNotExists(typeof(Candidate));
                db.CreateTableIfNotExists(typeof(CandidateInElection));
                db.CreateTableIfNotExists(typeof(VotingSlip));
                
                // Add Test data into the db
                foreach(var row in testDataElections)
                    db.Insert(row);
                foreach(var candidate in testDataCandidates)
                    db.Insert(candidate);
                foreach (var election in testDataRunning.Keys)
                    foreach (var candidate in testDataRunning[election])
                        db.Insert(new CandidateInElection() {CandidateId = candidate.Id, ElectionId = election.Id});
                
            }
        }

        [OneTimeTearDown]
        public void OneTimeTearDown() => appHost.Dispose();

        [Test]
        public void PostCandidateNotRunning()
        {
            var service = appHost.Container.Resolve<VotingSlipsService>();
            Assert.Throws<ArgumentException>(() =>
                service.Post(new VotingSlipsPostRequest() { Id = testDataElections[1].Id, 
                        Slip = new VotingSlip() {
                            Name = "bad slip",
                            Votes = new List<string>(){"Mrs. B"}
                        }
                    }
                ));
        }
        
        [Test]
        public void PostDuplicateNameTest()
        {
            var service = appHost.Container.Resolve<VotingSlipsService>();

            service.Post(new VotingSlipsPostRequest() {
                    Id = testDataElections[1].Id,
                    Slip = new VotingSlip() {
                        Name = "B-000",
                        Votes = new List<string>(){"Mr. A"}
                    }
                }
            );
                
            Assert.Throws<ArgumentException>(() =>
                service.Post(new VotingSlipsPostRequest() { Id = testDataElections[1].Id,
                        Slip = new VotingSlip() {
                            Name = "B-000",
                            Votes = new List<string>(){"Mr. A"}
                        }
                    }
                ));
        }
        
        [Test]
        public void PostAndGetTest()
        {
            var service = appHost.Container.Resolve<VotingSlipsService>();

            var election = testDataElections[0];

            VotingSlip[] slips = new VotingSlip[] {
                new VotingSlip() {
                    Name = "A-000",
                    Votes = new List<string>(){"Mr. A"}
                },
                new VotingSlip() {
                    Name = "A-001",
                    Votes = new List<string>(){"Mrs. B"}
                },
                new VotingSlip() {
                    Name = "A-002",
                    Votes = new List<string>(){"Mr. A", "Mrs. B"}
                },
                new VotingSlip() {
                    Name = "A-003",
                    Votes = new List<string>()
                },
            };
            foreach(var slip in slips)
                service.Post(new VotingSlipsPostRequest() {
                    Id = election.Id,
                    Slip = slip
                }
            );
            //Post a slip to another election to check that it doesn't come up
            service.Post(new VotingSlipsPostRequest() {
                Id = testDataElections[1].Id, 
                Slip = new VotingSlip() {Name = "C-000", Votes = new List<string>()
                }
            });

            var slipsReturned = service.Get(new VotingSlipsGetRequest() {Id = election.Id}).Slips;
            
            Assert.That(slipsReturned.Count, Is.EqualTo(slips.Length));
            foreach (var slip in slips) {
                Assert.True(slipsReturned.Contains(slip));
            }
        }
    }
}
