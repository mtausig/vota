//This file is part of Vota.
//
// Copyright 2019 Mathias Tausig <mathias@tausig.at>
//
//Vota is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Vota is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Vota.  If not, see <https://www.gnu.org/licenses/>.
using System.Linq;
using ServiceStack;
using ServiceStack.OrmLite;
using Vota.ServiceModel;
using Vota.ServiceModel.Types;

namespace Vota.ServiceInterface
{
    public class ElectionsCandidatesService : Service
    {
        public ElectionsCandidatesResponse Get(ElectionsCandidatesRequest request)
        {
            var query = Db.From<Candidate>()
                .Join<CandidateInElection>()
                .Join<CandidateInElection, Election>()
                .Where<Election>(e => e.Id == request.Id);
            var candidates = Db.Select<Candidate>(query).Select(c => c.ConvertTo<ElectionsCandidatesResponse.Candidate>());

                return new ElectionsCandidatesResponse() {Candidates = candidates.ToList()};
        }
    }
}
