//This file is part of Vota.
//
// Copyright 2019 Mathias Tausig <mathias@tausig.at>
//
//Vota is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Vota is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Vota.  If not, see <https://www.gnu.org/licenses/>.
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using ServiceStack;

namespace Vota.ServiceModel
{
    [Route("/elections/{id}/candidates")]
    [DataContract]
    public class ElectionsCandidatesRequest : IReturn<ElectionsCandidatesResponse>
    {
        public Guid Id { get; set; }
    }
    
    [DataContract]
    public class ElectionsCandidatesResponse
    {
        public class Candidate 
        {
            public String Name { get; set; }
        
            public bool EligibleForQuote { get; set; }

            protected bool Equals(Candidate other)
            {
                return string.Equals(Name, other.Name);
            }

            public override bool Equals(object obj)
            {
                if (ReferenceEquals(null, obj)) return false;
                if (ReferenceEquals(this, obj)) return true;
                if (obj.GetType() != this.GetType()) return false;
                return Equals((Candidate) obj);
            }

            public override int GetHashCode()
            {
                return (Name != null ? Name.GetHashCode() : 0);
            }


            public override string ToString()
            {
                return $"{nameof(Name)}: {Name}, {nameof(EligibleForQuote)}: {EligibleForQuote}";
            }
        }
        
        public List<Candidate> Candidates { get; set; }
        
    }
}
