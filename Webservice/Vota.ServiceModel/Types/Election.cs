//This file is part of Vota.
//
// Copyright 2019 Mathias Tausig <mathias@tausig.at>
//
//Vota is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Vota is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Vota.  If not, see <https://www.gnu.org/licenses/>.
using System;
using System.Runtime.Serialization;
using ServiceStack.DataAnnotations;
using Sing;

namespace Vota.ServiceModel.Types
{
    [DataContract]
    public class Election
    {
        [DataMember]
        [AutoId]
        public Guid Id { get; set; }
        
        [DataMember]
        [Required]
        public string Name { get; set; }
        
        [DataMember]
        [Required]
        public int MinCandidatesElected { get; set; }
        
        [DataMember]
        [Required]
        public int MaxCandidatesElected { get; set; }
        
        [DataMember]
        [Required]
        public ElectionType Type { get; set; }
    }
}