//This file is part of Vota.
//
// Copyright 2019 Mathias Tausig <mathias@tausig.at>
//
//Vota is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Vota is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Vota.  If not, see <https://www.gnu.org/licenses/>.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using ServiceStack.DataAnnotations;

namespace Vota.ServiceModel.Types
{
    [DataContract]
    public class VotingSlip
    {
        [DataMember]
        [Unique]
        [PrimaryKey]
        public String Name { get; set; }
        
        [DataMember]
        [Required]
        [References(typeof(Election))]
        public Guid ElectionId { get; set; }
        
        [DataMember]
        public List<String> Votes { get; set; }

        protected bool Equals(VotingSlip other)
        {
            return string.Equals(Name, other.Name) && Votes.SequenceEqual(other.Votes);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((VotingSlip) obj);
        }

        public override int GetHashCode()
        {
            unchecked {
                //TODO: HashCode for the Votes should come from the names since we are doing SequenceEqual above.
                return ((Name != null ? Name.GetHashCode() : 0) * 397) ^ (Votes != null ? Votes.GetHashCode() : 0);
            }
        }
    }
}