//This file is part of Vota.
//
// Copyright 2019 Mathias Tausig <mathias@tausig.at>
//
//Vota is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Vota is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Vota.  If not, see <https://www.gnu.org/licenses/>.
using System.Linq;

namespace Vota.ServiceModel
{
    /// <summary>
    /// Class to converse between Vota.Common classes and DB/DTO objects
    /// </summary>
    public static class VotaConversion
    {
        public static Vota.Common.Candidate ToCandidate(this Vota.ServiceModel.ElectionsCandidatesResponse.Candidate candidate)
        {
            return new Common.Candidate(candidate.Name, candidate.EligibleForQuote);
        }
        
        public static Vota.Common.Candidate ToCandidate(this Vota.ServiceModel.Types.Candidate candidate)
        {
            return new Common.Candidate(candidate.Name, candidate.EligibleForQuote);
        }
        
        public static Vota.Common.VotingSlip ToVotingSlip(this Vota.ServiceModel.Types.VotingSlip slip)
        {
            // We cannot set the eligibility for the quote here. Candidate need to be known in advance
            return new Common.VotingSlip(slip.Votes);
        }
    }
}