//This file is part of Vota.
//
// Copyright 2019 Mathias Tausig <mathias@tausig.at>
//
//Vota is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Vota is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Vota.  If not, see <https://www.gnu.org/licenses/>.
using System.Collections.Generic;
using Xunit;
using Vota.Common;


namespace Sing.Test
{
    public class RankedStvCounterTest
    {
        [Theory]
        [InlineData(true)]
        [InlineData(false)]  
        public void CalculateRankingTest(bool useCache)
        {
            Candidate[] candidates = new Candidate[] {
                new Candidate("Homer", false),
                new Candidate("Marge", true),
                new Candidate("Bart", false),
                new Candidate("Lisa", true),
                new Candidate("Maggie", true),
            };
            
            List<VotingSlip> votes = new List<VotingSlip>();
            for (int i = 0; i < 3; ++i)
                votes.Add(new VotingSlip("Homer", "Marge", "Lisa"));
            for(int i = 0; i < 4; ++i)
                votes.Add(new VotingSlip("Lisa", "Bart", "Marge"));
            for (int i = 0; i < 6; ++i)
                votes.Add(new VotingSlip("Bart", "Homer", "Lisa"));
            for (int i = 0; i < 5; ++i)
                votes.Add(new VotingSlip("Marge", "Bart", "Lisa", "Homer"));
            for (int i = 0; i < 6; ++i)
                votes.Add(new VotingSlip("Homer", "Marge", "Lisa"));
            
            // Manual Counting:
            // 1: B
            // 2: H, B
            // 3: H, M, B
            // 4: H, M, B, L
            // -> Ranked: 1. B, 2. M, 3. L, 4.H 

            RankedStvCounter counter = new RankedStvCounter(1, votes, useCache);
            counter.Candidates = candidates;
            var elected = counter.CalculateSeats().Elected;
            Assert.Equal(1, elected.Count);
            Assert.Equal(new Candidate("Bart"), elected[0]);

            counter = new RankedStvCounter(2, votes, useCache);
            counter.Candidates = candidates;
            elected = counter.CalculateSeats().Elected;
            Assert.Equal(2, elected.Count);
            Assert.Equal(new Candidate("Bart"), elected[0]);
            Assert.Equal(new Candidate("Marge"), elected[1]);
            
            counter = new RankedStvCounter(3, votes, useCache);
            counter.Candidates = candidates;
            elected = counter.CalculateSeats().Elected;
            Assert.Equal(3, elected.Count);
            Assert.Equal(new Candidate("Bart"), elected[0]);
            Assert.Equal(new Candidate("Marge"), elected[1]);
            Assert.Equal(new Candidate("Lisa"), elected[2]);
            
            counter = new RankedStvCounter(4, votes, useCache);
            counter.Candidates = candidates;
            elected = counter.CalculateSeats().Elected;
            Assert.Equal(4, elected.Count);
            Assert.Equal(new Candidate("Bart"), elected[0]);
            Assert.Equal(new Candidate("Marge"), elected[1]);
            Assert.Equal(new Candidate("Lisa"), elected[2]);
            Assert.Equal(new Candidate("Homer"), elected[3]);
        }

		[Theory]
		[InlineData(true)]
		[InlineData(false)]
		public void CalculateRankingWithPreelectedSeatsTest(bool useCache)
		{
			Candidate[] candidates = new Candidate[] {
				new Candidate("Homer", false),
				new Candidate("Marge", true),
				new Candidate("Bart", false),
				new Candidate("Lisa", true),
				new Candidate("Maggie", true),
			};

			List<VotingSlip> votes = new List<VotingSlip>();
			for (int i = 0; i < 3; ++i)
				votes.Add(new VotingSlip("Homer", "Marge", "Lisa"));
			for (int i = 0; i < 4; ++i)
				votes.Add(new VotingSlip("Lisa", "Bart", "Marge"));
			for (int i = 0; i < 6; ++i)
				votes.Add(new VotingSlip("Bart", "Homer", "Lisa"));
			for (int i = 0; i < 5; ++i)
				votes.Add(new VotingSlip("Marge", "Bart", "Lisa", "Homer"));
			for (int i = 0; i < 6; ++i)
				votes.Add(new VotingSlip("Homer", "Marge", "Lisa"));

			// Manual Counting:
			// 1: B
			// 2: H, B
			// 3: H, M, B
			// 4: H, M, B, L

			var elected = new RankedStvCounter(1, votes, useCache) {
				Candidates = candidates, SeatsPreelected = 1, SeatsPreelectedWithQuote = 1,
			}.CalculateSeats().Elected;
			Assert.Equal(1, elected.Count);
			Assert.Equal(new Candidate("Bart"), elected[0]);

			elected = new RankedStvCounter(1, votes, useCache) {
				Candidates = candidates, SeatsPreelected = 1, SeatsPreelectedWithQuote = 0,
			}.CalculateSeats().Elected;
			Assert.Equal(1, elected.Count);
			Assert.Equal(new Candidate("Marge"), elected[0]);

			elected = new RankedStvCounter(2, votes, useCache) {
				Candidates = candidates, SeatsPreelected = 5, SeatsPreelectedWithQuote = 3,
			}.CalculateSeats().Elected;
			Assert.Equal(2, elected.Count);
			Assert.Equal(new Candidate("Bart"), elected[0]);
			Assert.Equal(new Candidate("Marge"), elected[1]);

			elected = new RankedStvCounter(2, votes, useCache) {
				Candidates = candidates, SeatsPreelected = 2, SeatsPreelectedWithQuote = 1,
			}.CalculateSeats().Elected;
			Assert.Equal(2, elected.Count);
			Assert.Equal(new Candidate("Marge"), elected[0]);
			Assert.Equal(new Candidate("Bart"), elected[1]);

			elected = new RankedStvCounter(2, votes, useCache) {
				Candidates = candidates, SeatsPreelected = 1, SeatsPreelectedWithQuote = 0,
			}.CalculateSeats().Elected;
			Assert.Equal(2, elected.Count);
			Assert.Equal(new Candidate("Marge"), elected[0]);
			Assert.Equal(new Candidate("Lisa"), elected[1]);

			elected = new RankedStvCounter(3, votes, useCache) {
				Candidates = candidates, SeatsPreelected = 5, SeatsPreelectedWithQuote = 3,
			}.CalculateSeats().Elected;
			Assert.Equal(3, elected.Count);
			Assert.Equal(new Candidate("Bart"), elected[0]);
			Assert.Equal(new Candidate("Marge"), elected[1]);
			Assert.Equal(new Candidate("Homer"), elected[2]);

			elected = new RankedStvCounter(3, votes, useCache) {
				Candidates = candidates, SeatsPreelected = 1, SeatsPreelectedWithQuote = 0,
			}.CalculateSeats().Elected;
			Assert.Equal(3, elected.Count);
			Assert.Equal(new Candidate("Marge"), elected[0]);
			Assert.Equal(new Candidate("Lisa"), elected[1]);
			Assert.Equal(new Candidate("Bart"), elected[2]);

			elected = new RankedStvCounter(3, votes, useCache) {
				Candidates = candidates, SeatsPreelected = 2, SeatsPreelectedWithQuote = 2,
			}.CalculateSeats().Elected;
			Assert.Equal(3, elected.Count);
			Assert.Equal(new Candidate("Bart"), elected[0]);
			Assert.Equal(new Candidate("Homer"), elected[1]);
			Assert.Equal(new Candidate("Marge"), elected[2]);
		}

		[Theory]
		[InlineData(true)]
		[InlineData(false)]
		public void CalculateRankingWithPreelectedSeatsReducedQuorumTest(bool useCache)
		{
			Candidate[] candidates = new Candidate[] {
				new Candidate("Homer", false),
				new Candidate("Marge", true),
				new Candidate("Bart", false),
				new Candidate("Lisa", true),
				new Candidate("Maggie", true),
			};

			List<VotingSlip> votes = new List<VotingSlip>();
			for (int i = 0; i < 3; ++i)
				votes.Add(new VotingSlip("Homer", "Lisa"));
			for (int i = 0; i < 4; ++i)
				votes.Add(new VotingSlip("Lisa", "Homer", "Bart"));
			for (int i = 0; i < 6; ++i)
				votes.Add(new VotingSlip("Maggie"));
			for (int i = 0; i < 2; ++i)
				votes.Add(new VotingSlip("Marge", "Maggie"));
			for (int i = 0; i < 3; ++i)
				votes.Add(new VotingSlip("Marge"));

			// Manual Counting:
			// 1: -
			// 2: L, Mg
			// 3: L, Mg, Mr
			// 4: L, Mg, Mr, H

			var elected = new RankedStvCounter(3, votes, useCache) {
				Candidates = candidates, 
			}.CalculateSeats().Elected;
			Assert.Equal(0, elected.Count);

			elected = new RankedStvCounter(1, votes, useCache) {
				Candidates = candidates, SeatsPreelected = 1, SeatsPreelectedWithQuote = 1,
			}.CalculateSeats().Elected;
			Assert.Equal(1, elected.Count);
			// Tie breaker,Maggie has more first ranks
			Assert.Equal(new Candidate("Maggie"), elected[0]);

			System.IO.File.WriteAllText("/tmp/testprot.json", new RankedStvCounter(4, votes, useCache) {
				Candidates = candidates, SeatsPreelected = 2, SeatsPreelectedWithQuote = 1,
			}.CalculateSeats().Protocol.ToJson());
			elected = new RankedStvCounter(4, votes, useCache) {
				Candidates = candidates, SeatsPreelected = 1, SeatsPreelectedWithQuote = 1,
			}.CalculateSeats().Elected;
			Assert.Equal(3, elected.Count);
			// Tie breaker,Maggie has more first ranks
			Assert.Equal(new Candidate("Maggie"), elected[0]);
			Assert.Equal(new Candidate("Lisa"), elected[1]);
			Assert.Equal(new Candidate("Marge"), elected[2]);

			elected = new RankedStvCounter(4, votes, useCache) {
				Candidates = candidates, SeatsPreelected = 2, SeatsPreelectedWithQuote = 1,
			}.CalculateSeats().Elected;
			Assert.Equal(4, elected.Count);
			// Tie breaker,Maggie has more first ranks
			Assert.Equal(new Candidate("Maggie"), elected[0]);
			Assert.Equal(new Candidate("Lisa"), elected[1]);
			Assert.Equal(new Candidate("Marge"), elected[2]);
			Assert.Equal(new Candidate("Homer"), elected[3]);



		}

		[Theory]
        [InlineData(true)]
        [InlineData(false)]
        public void CalculateRankingNotAllSeatsTest(bool useCache)
        {
            Candidate[] candidates = new Candidate[] {
                new Candidate("Homer", false),
                new Candidate("Marge", true),
                new Candidate("Bart", false),
                new Candidate("Lisa", true),
            };
            
            List<VotingSlip> votes = new List<VotingSlip>();
            for (int i = 0; i < 3; ++i)
                votes.Add(new VotingSlip("Homer", "Marge"));
            for(int i = 0; i < 4; ++i)
                votes.Add(new VotingSlip("Lisa", "Bart", "Marge"));
            for (int i = 0; i < 6; ++i)
                votes.Add(new VotingSlip("Bart", "Homer"));
            for (int i = 0; i < 5; ++i)
                votes.Add(new VotingSlip("Marge", "Bart", "Homer"));
            for (int i = 0; i < 6; ++i)
                votes.Add(new VotingSlip("Homer", "Marge"));
            
            // Manual Counting:
            // 1: B
            // 2: H, B
            // 3: H, M, B
            // 4: H, M, B
            // -> Ranked: 1. B, 2. M 

            RankedStvCounter counter = new RankedStvCounter(1, votes, useCache);
            counter.Candidates = candidates;
            var elected = counter.CalculateSeats().Elected;
            Assert.Equal(1, elected.Count);
            Assert.Equal(new Candidate("Bart"), elected[0]);

            counter = new RankedStvCounter(2, votes, useCache);
            counter.Candidates = candidates;
            elected = counter.CalculateSeats().Elected;
            Assert.Equal(2, elected.Count);
            Assert.Equal(new Candidate("Bart"), elected[0]);
            Assert.Equal(new Candidate("Marge"), elected[1]);
            
            counter = new RankedStvCounter(3, votes, useCache);
            counter.Candidates = candidates;
            elected = counter.CalculateSeats().Elected;
            Assert.Equal(2, elected.Count);
            Assert.Equal(new Candidate("Bart"), elected[0]);
            Assert.Equal(new Candidate("Marge"), elected[1]);
            
            counter = new RankedStvCounter(4, votes, useCache);
            counter.Candidates = candidates;
            elected = counter.CalculateSeats().Elected;
            Assert.Equal(2, elected.Count);
            Assert.Equal(new Candidate("Bart"), elected[0]);
            Assert.Equal(new Candidate("Marge"), elected[1]);
        }
        
        [Theory]
        [InlineData(true)]
        [InlineData(false)]
        public void CalculateRankingAllSameWithQuoteReorderingTest(bool useCache)
        {
            Candidate[] candidates = new Candidate[] {
                new Candidate("Homer", false),
                new Candidate("Marge", true),
                new Candidate("Bart", false),
                new Candidate("Lisa", true),
            };
            
            List<VotingSlip> votes = new List<VotingSlip>();
            for (int i = 0; i < 100; ++i)
                votes.Add(new VotingSlip("Homer", "Bart", "Lisa", "Marge"));

            RankedStvCounter counter = new RankedStvCounter(1, votes, useCache);
            counter.Candidates = candidates;
            var elected = counter.CalculateSeats().Elected;
            Assert.Equal(1, elected.Count);
            Assert.Equal(new Candidate("Homer"), elected[0]);

            counter = new RankedStvCounter(2, votes, useCache);
            counter.Candidates = candidates;
            elected = counter.CalculateSeats().Elected;
            Assert.Equal(2, elected.Count);
            Assert.Equal(new Candidate("Homer"), elected[0]);
            Assert.Equal(new Candidate("Lisa"), elected[1]);
            
            counter = new RankedStvCounter(3, votes, useCache);
            counter.Candidates = candidates;
            elected = counter.CalculateSeats().Elected;
            Assert.Equal(3, elected.Count);
            Assert.Equal(new Candidate("Homer"), elected[0]);
            Assert.Equal(new Candidate("Lisa"), elected[1]);
            Assert.Equal(new Candidate("Marge"), elected[2]);
            
            counter = new RankedStvCounter(4, votes, useCache);
            counter.Candidates = candidates;
            elected = counter.CalculateSeats().Elected;
            Assert.Equal(4, elected.Count);
            Assert.Equal(new Candidate("Homer"), elected[0]);
            Assert.Equal(new Candidate("Lisa"), elected[1]);
            Assert.Equal(new Candidate("Marge"), elected[2]);
            Assert.Equal(new Candidate("Bart"), elected[3]);
        }
        
        [Theory]
        [InlineData(true)]
        [InlineData(false)]
        public void CalculateRankingTieBreakerFirstLevelVotesTest(bool useCache)
        {
            Candidate[] candidates = new Candidate[] {
                new Candidate("Marge", true),
                new Candidate("Lisa", true),
                new Candidate("Maggie", true),
                new Candidate("Bart", false),
            };
            
            List<VotingSlip> votes = new List<VotingSlip>();
            for (int i = 0; i < 8; ++i)
                votes.Add(new VotingSlip("Lisa", "Marge"));
            for (int i = 0; i < 10; ++i)
                votes.Add(new VotingSlip("Lisa", "Maggie"));
            for(int i = 0; i < 8; ++i)
                votes.Add(new VotingSlip("Bart"));
            for (int i = 0; i < 5; ++i)
                votes.Add(new VotingSlip("Marge", "Bart"));
            for (int i = 0; i < 4; ++i)
                votes.Add(new VotingSlip("Maggie", "Bart"));

            // Manual Counting:
            // Lisa has 18 Votes -> elected first
            // Quota for 3 seats is 9 -> 4 votes transferred to Marge, 5 to Maggie
            // -> Marge wins second seat because of more first level votes
            // Simple Counts:
            // 1: L
            // 2: L, B
            // 3: L, Mar, Mag
            // Ranking: L, B, Mar

            RankedStvCounter counter = new RankedStvCounter(3, votes, useCache);
            counter.Candidates = candidates;
            var elected = counter.CalculateSeats().Elected;
            Assert.Equal(3, elected.Count);
            Assert.Equal(new Candidate("Lisa"), elected[0]);
            Assert.Equal(new Candidate("Bart"), elected[1]);
            Assert.Equal(new Candidate("Marge"), elected[2]);
        }

		[Theory]
		[InlineData(true)]
		[InlineData(false)]
		public void CalculateSeatsWithTieBreakerWouldBreakQuoteTest(bool useCache)
		{
			Candidate[] candidates = new Candidate[] {
				new Candidate("Homer", false),
				new Candidate("Marge", true),
				new Candidate("Bart", false),
				new Candidate("Lisa", true),
				new Candidate("Maggie", true),
			};

			List<VotingSlip> votes = new List<VotingSlip>();
			for (int i = 0; i < 18; ++i)
				votes.Add(new VotingSlip("Homer"));
			for (int i = 0; i < 25; ++i)
				votes.Add(new VotingSlip("Bart", "Marge"));
			for (int i = 0; i < 17; ++i)
				votes.Add(new VotingSlip("Marge", "Homer"));

			// Manual Counting:
			// 1: H
			// 2: M, B (Bart owns tie breaker)
			// 3: B, M, H

			var elected = new RankedStvCounter(3, votes, useCache).CalculateSeats().Elected;
			Assert.Equal(2, elected.Count);
			Assert.Equal(new Candidate("Homer"), elected[0]);
			//Tie breaker should be ignored here due to the quote
			Assert.Equal(new Candidate("Marge"), elected[1]);

		}

		[Fact]
       public void CalculateSeatsWhereOnlyOneElectedForTwoSeatsTest()
       {
            Candidate[] candidates = new Candidate[] {
               new Candidate("Homer", false),
               new Candidate("Marge", true),
               new Candidate("Bart", false),
               new Candidate("Lisa", true),
               new Candidate("Maggie", true),
           };
           List<VotingSlip> votes = new List<VotingSlip>();
           for (int i = 0; i < 17; ++i)
               votes.Add(new VotingSlip("Homer"));
           for(int i = 0; i < 12; ++i)
               votes.Add(new VotingSlip("Lisa", "Homer"));
           for (int i = 0; i < 11; ++i)
               votes.Add(new VotingSlip("Bart", "Homer"));
            
           // Manual Counting:
           // 1: H
           // 2: H
           // 3: H, L, B
                      
           StvCounter counter = new RankedStvCounter(1, votes);
           counter.Candidates = candidates;
           var elected = counter.CalculateSeats().Elected;
           Assert.Equal(1, elected.Count);
           Assert.Equal(new Candidate("Homer", false), elected[0]);

           counter = new RankedStvCounter(2, votes);
           counter.Candidates = candidates;
           elected = counter.CalculateSeats().Elected;
           Assert.Equal(2, elected.Count);
           Assert.Equal(new Candidate("Homer", false), elected[0]);
           Assert.Equal(new Candidate("Lisa", false), elected[1]);

           counter = new RankedStvCounter(3, votes);
           counter.Candidates = candidates;
           elected = counter.CalculateSeats().Elected;
           Assert.Equal(2, elected.Count);
           Assert.Equal(new Candidate("Homer", false), elected[0]);
           Assert.Equal(new Candidate("Lisa", false), elected[1]);
       }

       [Fact]
       public void CalculateSeatsWithNoneElectedForFirstSeatTest()
       {
            Candidate[] candidates = new Candidate[] {
               new Candidate("Homer", false),
               new Candidate("Marge", true),
               new Candidate("Bart", false),
               new Candidate("Lisa", true),
               new Candidate("Maggie", true),
           };
           List<VotingSlip> votes = new List<VotingSlip>();
           for (int i = 0; i < 12; ++i)
               votes.Add(new VotingSlip("Homer"));
           for(int i = 0; i < 9; ++i)
               votes.Add(new VotingSlip("Lisa"));
           for (int i = 0; i < 9; ++i)
               votes.Add(new VotingSlip("Bart"));
            
           // Manual Counting:
           // 1: 
           // 2: H
           // 3: H, L, B
           // -> No one elected for ranked order
                      
           StvCounter counter = new RankedStvCounter(1, votes);
           counter.Candidates = candidates;
           var elected = counter.CalculateSeats().Elected;
           Assert.Equal(0, elected.Count);

           counter = new RankedStvCounter(2, votes);
           counter.Candidates = candidates;
           elected = counter.CalculateSeats().Elected;
           Assert.Equal(0, elected.Count);

           counter = new RankedStvCounter(3, votes);
           counter.Candidates = candidates;
           elected = counter.CalculateSeats().Elected;
           Assert.Equal(0, elected.Count);
       }

       [Fact]
        public void CalculateRankingWithNoneElectedForSecondSeatTest()
        {
            Candidate[] candidates = new Candidate[] {
                new Candidate("Marge", true),
                new Candidate("Lisa", true),
                new Candidate("Maggie", true),
                new Candidate("Bart", false),
            };
            
            List<VotingSlip> votes = new List<VotingSlip>();
            for (int i = 0; i < 10; ++i)
                votes.Add(new VotingSlip("Lisa", "Marge"));
            for(int i = 0; i < 5; ++i)
                votes.Add(new VotingSlip("Maggie"));
            for (int i = 0; i < 3; ++i)
                votes.Add(new VotingSlip("Bart"));

            // Manual Counting:
            // 1: L
            // 2: L
            // 3: L, Mar, Mag

            RankedStvCounter counter = new RankedStvCounter(1, votes);
            counter.Candidates = candidates;
            var elected = counter.CalculateSeats().Elected;
            Assert.Equal(1, elected.Count);
            Assert.Equal(new Candidate("Lisa"), elected[0]);

            counter = new RankedStvCounter(2, votes);
            counter.Candidates = candidates;
            elected = counter.CalculateSeats().Elected;
            Assert.Equal(1, elected.Count);
            Assert.Equal(new Candidate("Lisa"), elected[0]);

            counter = new RankedStvCounter(3, votes);
            counter.Candidates = candidates;
            elected = counter.CalculateSeats().Elected;
            Assert.Equal(1, elected.Count);
            Assert.Equal(new Candidate("Lisa"), elected[0]);
        }
        
        [Theory]
        [InlineData(true)]
        [InlineData(false)]  
        public void CalculateRankingWithCutoffTest(bool useCache)
        {
            Candidate[] candidates = new Candidate[] {
                new Candidate("Homer", false),
                new Candidate("Marge", true),
                new Candidate("Bart", false),
                new Candidate("Lisa", true),
                new Candidate("Maggie", true),
            };
            
            List<VotingSlip> votes = new List<VotingSlip>();
            for (int i = 0; i < 3; ++i)
                votes.Add(new VotingSlip("Homer", "Maggie", "Bart", "Marge"));
            for(int i = 0; i < 4; ++i)
                votes.Add(new VotingSlip("Marge", "Lisa", "Bart"));
            for (int i = 0; i < 6; ++i)
                votes.Add(new VotingSlip("Lisa", "Homer", "Marge"));
            for (int i = 0; i < 5; ++i)
                votes.Add(new VotingSlip("Bart", "Lisa", "Marge", "Homer"));
            for (int i = 0; i < 6; ++i)
                votes.Add(new VotingSlip("Homer", "Maggie", "Bart", "Marge"));
            
            // Manual Counting:
            // 1: L
            // 2: H, L
            // 3: H, B, L
            // 4: H, M, B, L
            // -> Ranked: 1. L, 2. H, 3. M, 4. B 
            //
            // Cutoff=3 -> no change
            // Cutoff = 2-> 1,2 no change, 3 only H, L, 4 only H,B,L
            //           -> 1. L 2. H
            //Cutoff = 1 -> empty list

            RankedStvCounter counter = new RankedStvCounter(4, votes, useCache) {
                CutOff = 4, Candidates = candidates,
            };
            var elected = counter.CalculateSeats().Elected;
            Assert.Equal(4, elected.Count);
            Assert.Equal(new Candidate("Lisa"), elected[0]);
            Assert.Equal(new Candidate("Homer"), elected[1]);
            Assert.Equal(new Candidate("Marge"), elected[2]);
            Assert.Equal(new Candidate("Bart"), elected[3]);
            
            counter = new RankedStvCounter(4, votes, useCache) {
                CutOff = 3, Candidates = candidates,
            };
            elected = counter.CalculateSeats().Elected;
            Assert.Equal(4, elected.Count);
            Assert.Equal(new Candidate("Lisa"), elected[0]);
            Assert.Equal(new Candidate("Homer"), elected[1]);
            Assert.Equal(new Candidate("Marge"), elected[2]);
            Assert.Equal(new Candidate("Bart"), elected[3]);
            
            counter = new RankedStvCounter(4, votes, useCache) {
                CutOff = 2, Candidates = candidates,
            };
            elected = counter.CalculateSeats().Elected;
            Assert.Equal(2, elected.Count);
            Assert.Equal(new Candidate("Lisa"), elected[0]);
            Assert.Equal(new Candidate("Homer"), elected[1]);
            
            counter = new RankedStvCounter(4, votes, useCache) {
                CutOff = 1, Candidates = candidates,
            };
            elected = counter.CalculateSeats().Elected;
            Assert.Equal(0, elected.Count);
        }

        [Fact]
        public void InvalidOrEmptySlipsShouldNotAffectQuotaTest()
        {
            var votes = CsvReader.ParseCsvList(@"Lisa
Lisa
Lisa
Lisa
Lisa
Lisa
Lisa

Marge
Marge

Maggie
Maggie

Bart

foo
foo
bar
bar
bla
blubb


");
            var candidates = new Candidate[]
            {
                new Candidate("Lisa", true), new Candidate("Marge", true),
                new Candidate("Maggie", true), new Candidate("Bart", false),
            };
            // We have 12 valid and a couple of invalid votes
            // Lisa should be only be electable for rank 1 if invalid votes are properly discarded
            var counter = new RankedStvCounter(1, votes) { Candidates = candidates };
            var elected = counter.CalculateSeats().Elected;
            Assert.Equal(1, elected.Count);
            Assert.True(elected.Contains(new Candidate("Lisa", true)));
        }
    }
}