//This file is part of Vota.
//
// Copyright 2019 Mathias Tausig <mathias@tausig.at>
//
//Vota is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Vota is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Vota.  If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using Vota.Common;
using Xunit;

namespace Sing.Test
{
   public class CountCacheTest
   {
       private static Candidate[] candidates = new Candidate[] {
               new Candidate("Homer", false),
               new Candidate("Marge", true),
               new Candidate("Bart", false),
               new Candidate("Lisa", true),
               new Candidate("Maggie", true),
           };
        private static List<VotingSlip> votes = new List<VotingSlip> {
            new VotingSlip("Homer", "Marge", "Lisa"),
            new VotingSlip("Homer", "Marge", "Lisa"),
            new VotingSlip("Marge", "Lisa"),
        };

       [Fact]
       public void AddResultNullCheckTest()
       {
           var counter = new StvCounter(1, votes);
           var cache = new CountCache();
           Assert.Throws<ArgumentNullException>( () => cache.AddResult(counter, null));
       }
       
       [Fact]
       public void GetFromEmptyCacheTest()
       {
           var counter = new StvCounter(1, votes);
           var cache = new CountCache();
           Assert.Null(cache.GetResult(counter));
       }

       [Fact]
       public void AddAndGetTest()
       {
            var cache = new CountCache();
            var counter1 = new StvCounter(1, votes);
            var result1 = new List<Candidate> { candidates[0] };
            cache.AddResult(counter1, result1);
            var cachedResult = cache.GetResult(counter1);
            Assert.NotNull(cachedResult);
            Assert.True(cachedResult.SequenceEqual(result1));
            var counter2 = new StvCounter(2, votes);
            var result2 = new List<Candidate> { candidates[0], candidates[3] };
            cache.AddResult(counter2, result2);
            cachedResult = cache.GetResult(counter2);
            Assert.NotNull(cachedResult);
            Assert.True(cachedResult.SequenceEqual(result2));
            //Getting the first result should still work
            cachedResult = cache.GetResult(counter1);
            Assert.NotNull(cachedResult);
            Assert.True(cachedResult.SequenceEqual(result1));
            //Getting a value for a different counter should not work
            var counter3 = new StvCounter(3, votes);
            cachedResult = cache.GetResult(counter3);
            Assert.Null(cachedResult);
       }

       [Fact]
       public void AddAndGetDifferentCounterInstanceTest()
       {
            var counter = new StvCounter(1, votes);
            var cache = new CountCache();
            var result = new List<Candidate> { candidates[0], candidates[3] };
            cache.AddResult(counter, result);
            counter = new StvCounter(1, votes);
            var cachedResult = cache.GetResult(counter);
            Assert.NotNull(cachedResult);
            Assert.True(cachedResult.SequenceEqual(result));
       }

         [Fact]
       public void AddAndGetDifferentCounterTypesTest()
       {
            var cache = new CountCache();
            StvCounter counter1 = new StvCounter(1, votes);
            var result1 = new List<Candidate> { candidates[0] };
            cache.AddResult(counter1, result1);
            var cachedResult = cache.GetResult(counter1);
            Assert.NotNull(cachedResult);
            Assert.True(cachedResult.SequenceEqual(result1));
            StvCounter counter2 = new QuotedStvCounter(2, votes);
            var result2 = new List<Candidate> { candidates[0], candidates[3] };
            cache.AddResult(counter2, result2);
            cachedResult = cache.GetResult(counter2);
            Assert.NotNull(cachedResult);
            Assert.True(cachedResult.SequenceEqual(result2));
            //Getting the first result should still work
            cachedResult = cache.GetResult(counter1);
            Assert.NotNull(cachedResult);
            Assert.True(cachedResult.SequenceEqual(result1));
            //Getting a value for a different counter should not work
            counter1 = new QuotedStvCounter(1, votes);
            Assert.Null(cache.GetResult(counter1));
            counter1 = new RankedStvCounter(1, votes);
            Assert.Null(cache.GetResult(counter1));
            counter2 = new StvCounter(2, votes);
            Assert.Null(cache.GetResult(counter2));
            counter2 = new RankedStvCounter(2, votes);
            Assert.Null(cache.GetResult(counter2));
       }

       [Fact]
       public void AddAndGetExplicitSeatNumbersTest()
       {
            var counter = new StvCounter(1, votes);
            var cache = new CountCache();
            var result1 = new List<Candidate> { candidates[0] };
            cache.AddResult(counter, result1);
            counter = new StvCounter(1, votes);
            var cachedResult = cache.GetResult(counter);
            Assert.NotNull(cachedResult);
            Assert.True(cachedResult.SequenceEqual(result1));
            cachedResult = cache.GetResult(counter, 1);
            Assert.NotNull(cachedResult);
            Assert.True(cachedResult.SequenceEqual(result1));

            var result2 = new List<Candidate> { candidates[0], candidates[3] };
            cache.AddResult(counter, result2, 2);
            cachedResult = cache.GetResult(counter);
            Assert.NotNull(cachedResult);
            Assert.True(cachedResult.SequenceEqual(result1));
            cachedResult = cache.GetResult(counter, 1);
            Assert.NotNull(cachedResult);
            Assert.True(cachedResult.SequenceEqual(result1));
            cachedResult = cache.GetResult(counter, 2);
            Assert.NotNull(cachedResult);
            Assert.True(cachedResult.SequenceEqual(result2));
            cachedResult = cache.GetResult(new StvCounter(2, votes));
            Assert.NotNull(cachedResult);
            Assert.True(cachedResult.SequenceEqual(result2));

       }

        [Fact]
        public void CacheIdentifierEqualsTest()
        {
            CountCache.CacheIdentifier id1 = new CountCache.CacheIdentifier()
            {
                Votes = votes, Seats = 42, Counter = typeof(StvCounter)
            };

            CountCache.CacheIdentifier id2 = new CountCache.CacheIdentifier()
            {
                Votes = votes.ToArray(),
                Seats = 42,
                Counter = typeof(StvCounter)
            };

            Assert.Equal(id1, id2);
            Assert.Equal(id1.GetHashCode(), id2.GetHashCode());
        }

        [Fact]
        public void CacheIdentifierEqualsFalseTest()
        {
            CountCache.CacheIdentifier id1 = new CountCache.CacheIdentifier()
            {
                Votes = new List<VotingSlip> { new VotingSlip("Marge", "Lisa") },
                Seats = 42,
                Counter = typeof(StvCounter)
            };

            CountCache.CacheIdentifier id2 = new CountCache.CacheIdentifier()
            {
                Votes = new List<VotingSlip> { new VotingSlip("Lisa", "Marge") },
                Seats = 42,
                Counter = typeof(StvCounter)
            };

            Assert.NotEqual(id1, id2);
            Assert.NotEqual(id1.GetHashCode(), id2.GetHashCode());

            id2 = new CountCache.CacheIdentifier()
            {
                Votes = new List<VotingSlip> { new VotingSlip("Marge", "Lisa") },
                Seats = 42,
                Counter = typeof(QuotedStvCounter),
            }; 
            Assert.NotEqual(id1, id2);
            Assert.NotEqual(id1.GetHashCode(), id2.GetHashCode());

            id2 = new CountCache.CacheIdentifier()
            {
                Votes = new List<VotingSlip> { new VotingSlip("Marge", "Lisa") },
                Seats = 43,
                Counter = typeof(StvCounter),
            };
            Assert.NotEqual(id1, id2);
            Assert.NotEqual(id1.GetHashCode(), id2.GetHashCode());
        }
    }
}