//This file is part of Vota.
//
// Copyright 2019 Mathias Tausig <mathias@tausig.at>
//
//Vota is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Vota is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Vota.  If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using Vota.Common;
using Xunit;

namespace Sing.Test
{
   public class QuotedStvCounterTest
   {
       [Theory]
       [InlineData(0, 0)]
       [InlineData(1, 0)]
       [InlineData(2, 1)]
       [InlineData(3, 2)]
       [InlineData(4, 2)]
       public void CalculateQuotedSeatsTest(uint seats, uint expectedQuotedSeats)
       {
           Assert.Equal(expectedQuotedSeats, QuotedStvCounter.CalculateQuotedSeats(seats));
       }

		[Theory]
		[InlineData(0, 0, 0, 0)]
		[InlineData(1, 0, 1, 1)]
		[InlineData(1, 1, 1, 0)]
		[InlineData(1, 1, 2, 1)]
		[InlineData(1, 0, 5, 5)]
		[InlineData(2, 1, 1, 1)]
		[InlineData(2, 2, 1, 0)]
		[InlineData(2, 1, 2, 1)]
		[InlineData(2, 0, 2, 2)]
		[InlineData(3, 1, 5, 3)]
		public void CalculateQuotedSeatsWithPreelectedTest(uint seats, uint expectedQuotedSeats, uint preelectedSeats, uint preelectedSeatsWithQuote)
		{
			Assert.Equal(expectedQuotedSeats, QuotedStvCounter.CalculateQuotedSeats(seats, preelectedSeats, preelectedSeatsWithQuote));
		}

       [Theory]
       [InlineData(0, 0, 0)]
       [InlineData(1, 0, 1)]
       [InlineData(1, 1, 0)]
       [InlineData(2, 2, 0)]
       [InlineData(2, 1, 1)]
       [InlineData(2, 0, 0)]
       [InlineData(3, 0, 0)]
       [InlineData(3, 1, 1)]
       [InlineData(3, 2, 1)]
       [InlineData(3, 3, 0)]
       public void CalculateOpenSeatsTest(uint seats, uint quotedSeatsElected, uint expectedOpenSeats)
       {
           Assert.Equal(expectedOpenSeats, QuotedStvCounter.CalculateOpenSeats(seats, quotedSeatsElected, 0, 0));
       }

		[Theory]
		[InlineData(0, 0, 0, 0, 0)]
		[InlineData(1, 0, 1, 0, 0)]
		[InlineData(1, 0, 1, 1, 1)]
		[InlineData(1, 0, 2, 1, 0)]
		[InlineData(1, 0, 2, 2, 1)]
		[InlineData(2, 0, 2, 2, 2)]
		[InlineData(2, 1, 1, 1, 1)]
		[InlineData(2, 1, 2, 1, 1)]
		[InlineData(3, 0, 2, 1, 0)]
		[InlineData(3, 0, 2, 2, 2)]
		[InlineData(1, 2, 2, 1, 0)]
		public void CalculateOpenSeatsWithPreelectedTest(uint seats, uint quotedSeatsElected, uint preelectedSeats, uint preelectedSeatsWithQuote, uint expectedOpenSeats)
		{
			Assert.Equal(expectedOpenSeats, QuotedStvCounter.CalculateOpenSeats(seats, quotedSeatsElected, preelectedSeats, preelectedSeatsWithQuote));
		}

		[Theory]
       [InlineData(true)]
       [InlineData(false)]
       public void CalculateSeatsWithQuoteTest(bool useCache)
       {
           Candidate[] candidates = new Candidate[] {
               new Candidate("Homer", false),
               new Candidate("Marge", true),
               new Candidate("Bart", false),
               new Candidate("Lisa", true),
               new Candidate("Maggie", true),
           };
           List<VotingSlip> votes = new List<VotingSlip>();
           for (int i = 0; i < 3; ++i)
               votes.Add(new VotingSlip("Homer", "Marge", "Lisa"));
           for(int i = 0; i < 4; ++i)
               votes.Add(new VotingSlip("Lisa", "Bart", "Marge"));
           for (int i = 0; i < 6; ++i)
               votes.Add(new VotingSlip("Bart", "Homer", "Lisa"));
           for (int i = 0; i < 5; ++i)
               votes.Add(new VotingSlip("Marge", "Bart", "Lisa", "Homer"));
           for (int i = 0; i < 6; ++i)
               votes.Add(new VotingSlip("Homer", "Marge", "Lisa"));
            
           // Manual Counting:
           // 1: B
           // 2: H, B
           // 3: H, M, B
           // 4: H, M, B, L
           
           StvCounter counter = new QuotedStvCounter(1, votes, useCache);
           counter.Candidates = candidates;
           var elected = counter.CalculateSeats().Elected;
           Assert.Single(elected);
           Assert.True(elected.Contains(new Candidate("Bart", false)));

           counter = new QuotedStvCounter(2, votes, useCache);
           counter.Candidates = candidates;
           elected = counter.CalculateSeats().Elected;
           Assert.Equal(2, elected.Count());
           Assert.True(elected.Contains(new Candidate("Marge", true)));
           Assert.True(elected.Contains(new Candidate("Bart", false)));
           
           counter = new QuotedStvCounter(3, votes, useCache);
           counter.Candidates = candidates;
           elected = counter.CalculateSeats().Elected;
           Assert.Equal(3, elected.Count());
           Assert.True(elected.Contains(new Candidate("Lisa", true)));
           Assert.True(elected.Contains(new Candidate("Marge", true)));
           Assert.True(elected.Contains(new Candidate("Bart", false)));
           
           counter = new QuotedStvCounter(4, votes, useCache);
           counter.Candidates = candidates;
           elected = counter.CalculateSeats().Elected;
           Assert.Equal(4, elected.Count());
           Assert.True(elected.Contains(new Candidate("Lisa", true)));
           Assert.True(elected.Contains(new Candidate("Marge", true)));
           Assert.True(elected.Contains(new Candidate("Bart", false)));
           Assert.True(elected.Contains(new Candidate("Homer", false)));
       }

        [Theory]
        [InlineData(true)]
        [InlineData(false)]
        public void CalculateSeatsWithQuoteAndPreelectedSeatsTest(bool useCache)
        {
            Candidate[] candidates = new Candidate[] {
               new Candidate("Homer", false),
               new Candidate("Marge", true),
               new Candidate("Bart", false),
               new Candidate("Lisa", true),
               new Candidate("Maggie", true),
           };
            List<VotingSlip> votes = new List<VotingSlip>();
            for (int i = 0; i < 3; ++i)
                votes.Add(new VotingSlip("Homer", "Marge", "Lisa"));
            for (int i = 0; i < 4; ++i)
                votes.Add(new VotingSlip("Lisa", "Bart", "Marge"));
            for (int i = 0; i < 6; ++i)
                votes.Add(new VotingSlip("Bart", "Homer", "Lisa"));
            for (int i = 0; i < 5; ++i)
                votes.Add(new VotingSlip("Marge", "Bart", "Lisa", "Homer"));
            for (int i = 0; i < 6; ++i)
                votes.Add(new VotingSlip("Homer", "Marge", "Lisa"));

            // Manual Counting:
            // 1: B
            // 2: H, B
            // 3: H, M, B
            // 4: H, M, B, L

            var counter = new QuotedStvCounter(2, votes, useCache) { SeatsPreelected = 1, SeatsPreelectedWithQuote = 0, 
                Candidates = candidates };
            var elected = counter.CalculateSeats().Elected;
            Assert.Equal(2, elected.Count());
            Console.WriteLine(elected[0]);
            Console.WriteLine(elected[1]);
            Assert.True(elected.Contains(new Candidate("Marge", true)));
            Assert.True(elected.Contains(new Candidate("Lisa", false)));

            elected = new QuotedStvCounter(2, votes, useCache)
            {
                SeatsPreelected = 1,
                SeatsPreelectedWithQuote = 1,
                Candidates = candidates
            }.CalculateSeats().Elected;
            Assert.Equal(2, elected.Count());
            Assert.True(elected.Contains(new Candidate("Bart", true)));
            Assert.True(elected.Contains(new Candidate("Marge", false)));

            elected = new QuotedStvCounter(2, votes, useCache)
            {
                SeatsPreelected = 2,
                SeatsPreelectedWithQuote = 1,
                Candidates = candidates
            }.CalculateSeats().Elected;
            Assert.Equal(2, elected.Count());
            Assert.True(elected.Contains(new Candidate("Bart", true)));
            Assert.True(elected.Contains(new Candidate("Marge", false)));

            elected = new QuotedStvCounter(2, votes, useCache)
            {
                SeatsPreelected = 2,
                SeatsPreelectedWithQuote = 2,
                Candidates = candidates
            }.CalculateSeats().Elected;
            Assert.Equal(2, elected.Count());
            Assert.True(elected.Contains(new Candidate("Bart", true)));
            Assert.True(elected.Contains(new Candidate("Homer", false)));

        }

		[Theory]
		[InlineData(true)]
		[InlineData(false)]
		public void CalculateSeatsWithTieBreakerRequiredTest(bool useCache)
		{
			Candidate[] candidates = new Candidate[] {
				new Candidate("Homer", false),
				new Candidate("Marge", true),
				new Candidate("Bart", false),
				new Candidate("Lisa", true),
				new Candidate("Maggie", true),
			};

			List<VotingSlip> votes = new List<VotingSlip>();
			for (int i = 0; i < 18; ++i)
				votes.Add(new VotingSlip("Homer"));
			for (int i = 0; i < 25; ++i)
				votes.Add(new VotingSlip("Marge", "Lisa"));
			for (int i = 0; i < 17; ++i)
				votes.Add(new VotingSlip("Lisa", "Homer"));

			// Manual Counting:
			// 1: H
			// 2: M, L
			// 3: L, M, H

			var elected = new QuotedStvCounter(1, votes, useCache) {
				Candidates = candidates, SeatsPreelected = 1, SeatsPreelectedWithQuote = 0,
			}.CalculateSeats().Elected;
			Assert.Single(elected);
			// Tie breaker,Marge has more first ranks
			Assert.Equal(new Candidate("Marge"), elected[0]);

		}

		[Theory]
       [InlineData(true)]
       [InlineData(false)]
       public void CalculateSeatsWithQuoteNotAllSeatsTest(bool useCache)
       {
           Candidate[] candidates = new Candidate[] {
               new Candidate("Homer", false),
               new Candidate("Marge", true),
               new Candidate("Bart", false),
               new Candidate("Lisa", true),
               new Candidate("Maggie", true),
           };
           List<VotingSlip> votes = new List<VotingSlip>();
           for (int i = 0; i < 3; ++i)
               votes.Add(new VotingSlip("Homer", "Marge"));
           for(int i = 0; i < 4; ++i)
               votes.Add(new VotingSlip("Lisa", "Bart", "Marge"));
           for (int i = 0; i < 6; ++i)
               votes.Add(new VotingSlip("Bart", "Homer"));
           for (int i = 0; i < 5; ++i)
               votes.Add(new VotingSlip("Marge", "Bart", "Homer"));
           for (int i = 0; i < 6; ++i)
               votes.Add(new VotingSlip("Homer", "Marge"));
            
           // Manual Counting:
           // 1: B
           // 2: H, B
           // 3: H, M, B
           // 4: H, M, B
           
           StvCounter counter = new QuotedStvCounter(1, votes, useCache);
           counter.Candidates = candidates;
           var elected = counter.CalculateSeats().Elected;
           Assert.Single(elected);
           Assert.True(elected.Contains(new Candidate("Bart", false)));

           counter = new QuotedStvCounter(2, votes, useCache);
           counter.Candidates = candidates;
           elected = counter.CalculateSeats().Elected;
           Assert.Equal(2, elected.Count());
           Assert.True(elected.Contains(new Candidate("Marge", true)));
           Assert.True(elected.Contains(new Candidate("Bart", false)));
           
           counter = new QuotedStvCounter(3, votes, useCache);
           counter.Candidates = candidates;
           elected = counter.CalculateSeats().Elected;
           Assert.Equal(2, elected.Count());
           Assert.True(elected.Contains(new Candidate("Marge", true)));
           Assert.True(elected.Contains(new Candidate("Bart", false)));
           
           counter = new QuotedStvCounter(4, votes, useCache);
           counter.Candidates = candidates;
           elected = counter.CalculateSeats().Elected;
           Assert.Equal(2, elected.Count());
           Assert.True(elected.Contains(new Candidate("Marge", true)));
           Assert.True(elected.Contains(new Candidate("Bart", false)));
       }

       [Fact]
       public void CalculateSeatsWithQuoteOnlyOneWithoutQuoteElectedForTwoSeatsTest()
       {
            Candidate[] candidates = new Candidate[] {
               new Candidate("Homer", false),
               new Candidate("Marge", true),
               new Candidate("Bart", false),
               new Candidate("Lisa", true),
               new Candidate("Maggie", true),
           };
           List<VotingSlip> votes = new List<VotingSlip>();
           for (int i = 0; i < 17; ++i)
               votes.Add(new VotingSlip("Homer"));
           for(int i = 0; i < 12; ++i)
               votes.Add(new VotingSlip("Lisa", "Homer"));
           for (int i = 0; i < 11; ++i)
               votes.Add(new VotingSlip("Bart", "Homer"));
            
           // Manual Counting:
           // 1: H
           // 2: H
           // 3: H, L, B
                      
           StvCounter counter = new QuotedStvCounter(1, votes);
           counter.Candidates = candidates;
           var elected = counter.CalculateSeats().Elected;
           Assert.Single(elected);
           Assert.True(elected.Contains(new Candidate("Homer", false)));

           counter = new QuotedStvCounter(2, votes);
           counter.Candidates = candidates;
           elected = counter.CalculateSeats().Elected;
           Assert.Equal(2, elected.Count());
           Assert.True(elected.Contains(new Candidate("Homer", true)));
           Assert.True(elected.Contains(new Candidate("Lisa", false)));
           
           counter = new QuotedStvCounter(3, votes);
           counter.Candidates = candidates;
           elected = counter.CalculateSeats().Elected;
           Assert.Equal(2, elected.Count());
           Assert.True(elected.Contains(new Candidate("Homer", true)));
           Assert.True(elected.Contains(new Candidate("Lisa", true)));
       }

       [Fact]
       public void CalculateSeatsWithQuoteOnlyOneWithQuoteElectedForTwoSeatsTest()
       {
            Candidate[] candidates = new Candidate[] {
               new Candidate("Homer", false),
               new Candidate("Marge", true),
               new Candidate("Bart", false),
               new Candidate("Lisa", true),
               new Candidate("Maggie", true),
           };
           List<VotingSlip> votes = new List<VotingSlip>();
           for (int i = 0; i < 17; ++i)
               votes.Add(new VotingSlip("Lisa"));
           for(int i = 0; i < 12; ++i)
               votes.Add(new VotingSlip("Homer", "Lisa"));
           for (int i = 0; i < 11; ++i)
               votes.Add(new VotingSlip("Marge", "Lisa"));
            
           // Manual Counting:
           // 1: L
           // 2: L
           // 3: H, L, M
           // Expected quoted:
           // 1: L
           // 2: L
           // 3: H, L, M
                      
           StvCounter counter = new QuotedStvCounter(1, votes);
           counter.Candidates = candidates;
           var elected = counter.CalculateSeats().Elected;
           Assert.Single(elected);
           Assert.True(elected.Contains(new Candidate("Lisa", false)));

           counter = new QuotedStvCounter(2, votes);
           counter.Candidates = candidates;
           elected = counter.CalculateSeats().Elected;
           Assert.Single(elected);
           Assert.True(elected.Contains(new Candidate("Lisa", true)));
           
           counter = new QuotedStvCounter(3, votes);
           counter.Candidates = candidates;
           elected = counter.CalculateSeats().Elected;
           Assert.Equal(3, elected.Count());
           Assert.True(elected.Contains(new Candidate("Homer", true)));
           Assert.True(elected.Contains(new Candidate("Lisa", true)));
           Assert.True(elected.Contains(new Candidate("Marge", true)));
       }

       [Fact]
       public void CalculateSeatsWithQuoteNoneElectedForFirstSeatTest()
       {
            Candidate[] candidates = new Candidate[] {
               new Candidate("Homer", false),
               new Candidate("Marge", true),
               new Candidate("Bart", false),
               new Candidate("Lisa", true),
               new Candidate("Maggie", true),
           };
           List<VotingSlip> votes = new List<VotingSlip>();
           for (int i = 0; i < 12; ++i)
               votes.Add(new VotingSlip("Homer"));
           for(int i = 0; i < 9; ++i)
               votes.Add(new VotingSlip("Lisa"));
           for (int i = 0; i < 9; ++i)
               votes.Add(new VotingSlip("Bart"));
            
           // Manual Counting:
           // 1: 
           // 2: H
           // 3: H, L, B
           // Expected Quoted result: 
           // 1: none
           // 2: H, L
           // 3: H, L
                      
           StvCounter counter = new QuotedStvCounter(1, votes);
           counter.Candidates = candidates;
           var elected = counter.CalculateSeats().Elected;
           Assert.Empty(elected);

           counter = new QuotedStvCounter(2, votes);
           counter.Candidates = candidates;
           elected = counter.CalculateSeats().Elected;
           Assert.Equal(2, elected.Count());
           Assert.True(elected.Contains(new Candidate("Homer", true)));
           Assert.True(elected.Contains(new Candidate("Lisa", false)));
           
           counter = new QuotedStvCounter(3, votes);
           counter.Candidates = candidates;
           elected = counter.CalculateSeats().Elected;
           Assert.Equal(2, elected.Count());
           Assert.True(elected.Contains(new Candidate("Homer", true)));
           Assert.True(elected.Contains(new Candidate("Lisa", true)));
       }

       [Fact]
       public void CalculateSeatsWithQuoteRequiringManyAdditionalCountsTest()
       {
            Candidate[] candidates = new Candidate[] {
               new Candidate("Homer", false),
               new Candidate("Marge", true),
               new Candidate("Bart", false),
               new Candidate("Lisa", true),
               new Candidate("Moe", false),
           };
           List<VotingSlip> votes = new List<VotingSlip>();
           for (int i = 0; i < 18; ++i)
               votes.Add(new VotingSlip("Lisa", "Marge", "Moe", "Bart"));
           for(int i = 0; i < 13; ++i)
               votes.Add(new VotingSlip("Homer", "Lisa", "Bart"));
           for (int i = 0; i < 11; ++i)  
               votes.Add(new VotingSlip("Moe", "Lisa", "Homer"));
           for (int i = 0; i < 8; ++i)  
               votes.Add(new VotingSlip("Bart"));
            
           // Manual Counting:
           // 1: L
           // 2: H, L
           // 3: H, L, Mo
           // 4: H, L, Mo, B
           // 5: H, L, Mo, B, Mar
           // Expected Quoted result: 
           // 1: L
           // 2: H, L
           // 3: H, L, Mar
                      
           StvCounter counter = new QuotedStvCounter(1, votes);
           counter.Candidates = candidates;
           var elected = counter.CalculateSeats().Elected;
           Assert.Single(elected);
           Assert.True(elected.Contains(new Candidate("Lisa", true)));

           counter = new QuotedStvCounter(2, votes);
           counter.Candidates = candidates;
           elected = counter.CalculateSeats().Elected;
           Assert.Equal(2, elected.Count());
           Assert.True(elected.Contains(new Candidate("Homer", true)));
           Assert.True(elected.Contains(new Candidate("Lisa", false)));
           
           counter = new QuotedStvCounter(3, votes);
           counter.Candidates = candidates;
           elected = counter.CalculateSeats().Elected;
           Assert.Equal(3, elected.Count());
           Assert.True(elected.Contains(new Candidate("Homer", true)));
           Assert.True(elected.Contains(new Candidate("Lisa", true)));
           Assert.True(elected.Contains(new Candidate("Marge", true)));
       }

        [Fact]
        public void InvalidOrEmptySlipsShouldNotAffectQuotaTest()
        {
            var votes = CsvReader.ParseCsvList(@"Lisa
Lisa
Lisa
Lisa
Lisa

Marge
Marge
Marge

Maggie
Maggie
Maggie

Bart

foo
foo
bar
bar
bla
blubb


");
            var candidates = new Candidate[]
            {
                new Candidate("Lisa", true), new Candidate("Marge", true),
                new Candidate("Maggie", true), new Candidate("Bart", false),
            };
            // We have 12 valid and a couple of invalid votes
            // Lisa should be the only one elected for 2 seats (Quota 5), if invalid votes are properly discarded
            var counter = new QuotedStvCounter(2, votes) { Candidates = candidates };
            var elected = counter.CalculateSeats().Elected;
            Assert.Single(elected);
            Assert.True(elected.Contains(new Candidate("Lisa", true)));
        }
    } 
}