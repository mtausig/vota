//This file is part of Vota.
//
// Copyright 2019 Mathias Tausig <mathias@tausig.at>
//
//Vota is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Vota is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Vota.  If not, see <https://www.gnu.org/licenses/>.
using Xunit;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit.Abstractions;
using Vota.Common;

namespace Sing.Test 
{
    public class StvCounterTest
    {
		private class StvCounterWithPublicCache : StvCounter
		{
			public StvCounterWithPublicCache(uint seats, IEnumerable<VotingSlip> votes, bool useCache) : base(seats, votes, useCache)
			{
			}

			public CountCache Cache { get => cache; }
		}

		private readonly ITestOutputHelper _testOutputHelper;

        public StvCounterTest(ITestOutputHelper testOutputHelper)
        {
            _testOutputHelper = testOutputHelper;
        }

        [Fact]
        public void StvCounterSingleSeatWithoutTransferTest()
        {
            var votes = CsvReader.ParseCsvList(@"John;Paul
John;Paul
John;Paul
John;Ringo
John;Ringo
Ringo; Paul; John
Ringo; Paul; John
Ringo; Paul; John
Ringo; Paul; John");
            StvCounter counter = new StvCounter(1, votes);
            var elected = counter.CalculateSeats().Elected;
            Assert.Single(elected);
            Assert.Equal("John", elected.First().Name);
        }

        [Fact]
        public void StvCounterSingleSeatWithOneTransferFromLooserTest()
        {
            var votes = CsvReader.ParseCsvList(@"John;Paul
John;Paul
John;Paul
John;Ringo
Paul;John
Ringo; Paul; John
Ringo; Paul; John
Ringo; Paul; John
Ringo; Paul; John");
            StvCounter counter = new StvCounter(1, votes);
            var elected = counter.CalculateSeats().Elected;
            Assert.Single(elected);
            Assert.Equal("John", elected.First().Name);
        }

        [Fact]
        public void StvCounterTwoSeatWithMultipleTransfersFromLoosersTest()
        {
            var votes = CsvReader.ParseCsvList(@"John;Paul
John;Paul
John;Paul
George;Ringo
George;John
Paul;John
Ringo; Paul; John
Ringo; Paul; John
Ringo; Paul; John
");
/*
Quota = 4
Step 1: John 3, Ringo 3, George 2, Paul 1
    Remove Paul -> John+1
Step 2: John 4, Ringo 3, George 2
    John elected, no suprplus votes
Step 3: Ringo 3, George 2
    Remove George -> Ringo +1
Step 4: Ringo 4
    Ringo elected
 */
            StvCounter counter = new StvCounter(2, votes);
            var elected = counter.CalculateSeats().Elected;
            Assert.Equal(2, elected.Count());
            Assert.True(elected.Contains(new Candidate("John")));
            Assert.True(elected.Contains(new Candidate("Ringo")));
        }

        [Fact]
        public void StvCounterTwoSeatWithSurplusTest()
        {
            var votes = CsvReader.ParseCsvList(@"John;Paul
John;Paul
John;Paul
John;Paul
John;Paul
John;Paul
John;Ringo
John;Ringo
George;Paul
Paul;John
Ringo; Paul; John
Ringo; Paul; John
Ringo; Paul; John
");
/*
13 votes, Quota = 5
Step 1: John 8, Ringo 3, George 1, Paul 1
    John elected -> transfer 3/8 of the votes
Step 2: Ringo 3 3/4, Paul 3 1/4, George 1
    remove george -> Paul+1
Step 3: Paul 4 1/14, Ringo 3 1/4
    remove Ringo -> Paul +3
Step 4: Paul 7 1/4
    Paul elected
 */
            StvCounter counter = new StvCounter(2, votes);
            var elected = counter.CalculateSeats().Elected;
            Assert.Equal(2, elected.Count());
            Assert.True(elected.Contains(new Candidate("John")));
            Assert.True(elected.Contains(new Candidate("Paul")));
        }

        [Fact]
        public void CandidatesDefaultValueIsNullTest ()
        {
            StvCounter c = new StvCounter(1, new VotingSlip[0]);
            Assert.Null(c.Candidates);
        }

        [Fact]
        public void CandidatesSetGetTest()
        {
            StvCounter c = new StvCounter(1, new VotingSlip[0]);
            Candidate[] candidates = new Candidate[] {new Candidate("foo"), new Candidate("bar")};
            c.Candidates = candidates;
            Assert.True(c.Candidates.SequenceEqual(candidates));
        }

        [Fact]
        public void CandidatesSetUniqueTest()
        {
            StvCounter c = new StvCounter(1, new VotingSlip[0]);
            Candidate[] candidates = new Candidate[] {new Candidate("foo"), new Candidate("foo")};
            c.Candidates = candidates;
            Assert.True(c.Candidates.SequenceEqual(new Candidate[]{new Candidate("foo")}), 
                "Duplicate candidates should be removed by the setter.");
        }

        [Fact]
        public void CalculateSeatsWithRestrictCandidatesTest()
        {
            var votes = CsvReader.ParseCsvList(@"John;Paul
John;Paul
John;Paul
John;Ringo
John;Ringo
Ringo; Paul; John
Ringo; Paul; John
Ringo; Paul; John
Ringo; Paul; John");
            StvCounter counter = new StvCounter(1, votes);
            counter.Candidates = new Candidate[]{new Candidate("Paul")};
            var elected = counter.CalculateSeats().Elected;
            Assert.True(elected.SequenceEqual(new Candidate[]{new Candidate("Paul")}));

            counter.Candidates = new Candidate[]{new Candidate("Paul"), new Candidate("Ringo")};
            elected = counter.CalculateSeats().Elected;
            Assert.True(elected.SequenceEqual(new Candidate[]{new Candidate("Ringo")}));

            counter.Candidates = new Candidate[]{new Candidate("George")};
            elected = counter.CalculateSeats().Elected;
            Assert.False(elected.Any());
        }

        [Fact]
        public void CalculateSeatsWithTieTest()
        {
            var votes = CsvReader.ParseCsvList(@"John
John
John
John
George; Paul
George; Paul
Paul; John
Paul; John
Paul; John
Ringo; George; Paul");
            // 1 Seat (Q6): 
            // First count: J 4, P 3, G 2, R 1 -> R out
            // Second count: J 4, P 3, G 3 -> G out because P had more votes before
            // Third count: J 4, P 6 -> P wins single seat
            // 2 Seat (Q4):
            // First count: J 4, P 3, G 2, R 1 -> J wins seat
            // Second count: P 3, G 3, R 1 -> R out
            // Third count: R 3, G 3 -> G out (tiebreaker)
            //Fourth count: P6 -> wins seat
            
            StvCounter counter = new StvCounter(1, votes);
            var elected = counter.CalculateSeats().Elected;
            Assert.True(elected.SequenceEqual(new Candidate[]{new Candidate("Paul")}));
            
            counter = new StvCounter(2, votes);
            elected = counter.CalculateSeats().Elected;
            Assert.True(elected.Contains(new Candidate("Paul")));
            Assert.True(elected.Contains(new Candidate("John")));
        }
        
        [Fact]
        public void CalculateSeatsWithTieOnSecondLevelTest()
        {
            var votes = CsvReader.ParseCsvList(@"John
John
John
John
John
John
Paul; John
Paul; John
Paul; John
George; Paul
George; Paul
George; Paul
Ringo; George; Paul
Ringo
Mick; Paul");
            // 1 Seat (Q8): 
            // First count: J 6, P 3, G 3, R 2, M 1 -> M out
            // Second count: J 6, P 4, G 3 R 2 -> R out
            // Third count: J 6, P 4 G 4 -> G out because P hat 4 votes earlier
            // Fourth count: J 6 P8 -> P wins 
            
            StvCounter counter = new StvCounter(1, votes);
            var elected = counter.CalculateSeats().Elected;
            Assert.True(elected.SequenceEqual(new Candidate[]{new Candidate("Paul")}));
        }

        [Fact]
        public void CalculateSeatsWithTieRandomChoiceTest()
        {
            var votes = CsvReader.ParseCsvList(@"John;Paul
Paul;John");


            StvCounter counter = new StvCounter(1, votes);
            var elected = counter.CalculateSeats().Elected;
            Assert.Single(elected);
            // Lets run the test a couple of times to check if both candidates win from time to time
            Dictionary<Candidate, bool> haveWon = new Dictionary<Candidate, bool>();
            haveWon[elected.First()] = true;
            // We stop at 1024 runs, it it extremely unlikely (1/2^{1024}) that we will get a false negative
            for (int i = 0; i < 1024; ++i) {
                elected = counter.CalculateSeats().Elected;
                haveWon[elected.First()] = true;
                if (haveWon.Count == 2)
                    break;
            }

            Assert.Equal(2, haveWon.Count);
        }

        [Fact]
        public void CalculateSeatsWithTieRandomChoiceIsCachedTest()
        {
            var votes = CsvReader.ParseCsvList(@"John;Paul
Paul;John");


            StvCounter counter = new StvCounter(1, votes);
            counter.CreateCache();
            var elected = counter.CalculateSeats().Elected;
            Assert.Single(elected);
            // Lets run the test a couple of times to check if result stays the same due to the cache
            // We stop at 128 runs, it it extremely unlikely (1/2^{128}) that we will get a false negative
            for (int i = 0; i < 128; ++i) {
                var cachedResult = counter.CalculateSeats().Elected;
                Assert.True(cachedResult.SequenceEqual(elected));
            }
        }

        [Fact]
        public void CalculateSeatsWithCutoffTest()
        {
            var votes = CsvReader.ParseCsvList(@"John;Paul
John;Paul
John;Paul
George;John;Paul;Ringo
George;John;Ringo
Paul;John
Ringo; Paul; John
Ringo; Paul; John
Ringo; Paul; John
");
/*
9 votes, 2 seats, Quota = 4
Step 1: John 3, Ringo 3, George 2, Paul 1
    Remove Paul -> John+1
Step 2: John 4, Ringo 3, George 2
    John elected, no surplus votes
Step 3: Ringo 3, George 2
    Remove George -> Ringo +1
Step 4: Ringo 5
    Ringo elected
    
    Cutoff = 3 -> same result
    Cutoff = 2 -> only John elected
 */
                StvCounter counter = new StvCounter(2, votes);
                var elected = counter.CalculateSeats().Elected;
                Assert.Equal(2, elected.Count());
                Assert.True(elected.Contains(new Candidate("John")));
                Assert.True(elected.Contains(new Candidate("Ringo")));
                
                //CutOff = 0 should not change anything
                counter = new StvCounter(2, votes) {CutOff = 0};
                elected = counter.CalculateSeats().Elected;
                Assert.Equal(2, elected.Count());
                Assert.True(elected.Contains(new Candidate("John")));
                Assert.True(elected.Contains(new Candidate("Ringo")));
                
                //CutOff < 0 should not change anything
                counter = new StvCounter(2, votes) {CutOff = -1};
                elected = counter.CalculateSeats().Elected;
                Assert.Equal(2, elected.Count());
                Assert.True(elected.Contains(new Candidate("John")));
                Assert.True(elected.Contains(new Candidate("Ringo")));
                
                //CutOff = 4 should not change anything
                counter = new StvCounter(2, votes) {CutOff = 4};
                elected = counter.CalculateSeats().Elected;
                Assert.Equal(2, elected.Count());
                Assert.True(elected.Contains(new Candidate("John")));
                Assert.True(elected.Contains(new Candidate("Ringo")));
                
                //CutOff = 3 should not change anything
                counter = new StvCounter(2, votes) {CutOff = 0};
                elected = counter.CalculateSeats().Elected;
                Assert.Equal(2, elected.Count());
                Assert.True(elected.Contains(new Candidate("John")));
                Assert.True(elected.Contains(new Candidate("Ringo")));
                
                //CutOff = 2 -> Ringo not elected
                counter = new StvCounter(2, votes) {CutOff = 2};
                elected = counter.CalculateSeats().Elected;
                Assert.Single(elected);
                Assert.True(elected.Contains(new Candidate("John")));
                
                //CutOff = 1 should not change anything
                counter = new StvCounter(2, votes) {CutOff = 1};
                elected = counter.CalculateSeats().Elected;
                Assert.Empty(elected);
        }

        [Fact]
        public void InvalidOrEmptySlipsShouldNotAffectQuotaTest()
        {
            var votes = CsvReader.ParseCsvList(@"John
John
John
John

George
George

Paul
Paul
Paul
Paul 

Ringo
Ringo


foo
foo
bar
bar
bla
blubb


");
            var candidates = new Candidate[]
            {
                new Candidate("John"), new Candidate("Paul"),
                new Candidate("George"), new Candidate("Ringo"),
            };
            // We have 12 valid and a couple of invalid votes
            // John and Paul should be the only one elected for 4 seats (Quota 3), if invalid votes are properly discarded
            StvCounter counter = new StvCounter(4, votes) { Candidates = candidates };
            var elected = counter.CalculateSeats().Elected;
            Assert.Equal(2, elected.Count);
            Assert.True(elected.Contains(new Candidate("John")));
            Assert.True(elected.Contains(new Candidate("Paul")));
        }

        [Fact]
		public void CalculateSeatsCacheUsageTest()
		{
			var votes = CsvReader.ParseCsvList(@"John;Paul
John;Paul
John;Paul
John;Paul;George");
			var counter = new StvCounterWithPublicCache(1, votes, true);
			//Add a faked cached result and see if it gets returned
			counter.Cache.AddResult(typeof(StvCounter), new Candidate[] { new Candidate("Paul") }, 1, counter.Votes);
			var result = counter.CalculateSeats();
			Assert.Single(result.Elected);
			Assert.Equal("Paul", result.Elected[0].Name);
		}
	}
}